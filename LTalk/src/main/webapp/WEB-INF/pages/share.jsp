﻿<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${piazza.get("content")}</title>
<meta charset="utf-8">
<meta name="viewport"
	content="initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
<link rel="stylesheet" href="../css/app.css">
<script src="../js/jquery.min.js" type="text/javascript"></script>

<style type="text/css">
* {
	cursor: pointer;
}

.weui_mask_transition {
	display: none;
	position: fixed;
	z-index: 1;
	width: 100%;
	height: 100%;
	top: 0;
	left: 0;
	background: rgba(0, 0, 0, 0);
	-webkit-transition: background .3s;
	transition: background .3s;
}

.weui_fade_toggle {
	background: rgba(0, 0, 0, 0.6);
}

.weui_actionsheet {
	position: fixed;
	left: 0;
	bottom: 0;
	-webkit-transform: translate(0, 100%);
	-ms-transform: translate(0, 100%);
	transform: translate(0, 100%);
	-webkit-backface-visibility: hidden;
	backface-visibility: hidden;
	z-index: 2;
	width: 100%;
	background-color: #EFEFF4;
	-webkit-transition: -webkit-transform .3s;
	transition: transform .3s;
}

.weui_actionsheet_toggle {
	-webkit-transform: translate(0, 0);
	-ms-transform: translate(0, 0);
	transform: translate(0, 0);
}

.weui_actionsheet_menu {
	background-color: #FFFFFF;
}

.weui_actionsheet_cell:before {
	content: " ";
	position: absolute;
	left: 0;
	top: 0;
	width: 100%;
	height: 1px;
	border-top: 1px solid #D9D9D9;
	-webkit-transform-origin: 0 0;
	-ms-transform-origin: 0 0;
	transform-origin: 0 0;
	-webkit-transform: scaleY(0.5);
	-ms-transform: scaleY(0.5);
	transform: scaleY(0.5);
}

.weui_actionsheet_cell:first-child:before {
	display: none;
}

.weui_actionsheet_cell {
	position: relative;
	padding: 10px 0;
	text-align: center;
	font-size: 18px;
}

.weui_actionsheet_cell.title {
	color: #999;
}

.weui_actionsheet_action {
	margin-top: 6px;
	background-color: #FFFFFF;
}
</style>

</head>
<body>
	<div id="main">
		<div id="list">
			<ul>
				<li>
					<div class="po-avt-wrap">
						<img class="po-avt data-avt" src="${piazza.get("poster").get("avatar")}">
					</div>
					<div class="po-cmt">
						<div class="po-hd">
							<p class="po-name">
								<span class="data-name">${piazza.get("poster").get("nickname")}</span>
							</p>
							<div class="post">
								<p>${piazza.get("content")}</p>
								<p>
								    <c:forEach var="image" items='${piazza.get("images")}'>
									   <img class="list-img" src="${image}" style="height: 100px;">
									</c:forEach>
								</p>
							</div>
							<p class="time">${piazza.get("time")}</p>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
    <center>Power By <a href="http://114.115.140.156/ltalk/ltalk.1.0.apk">LTalk</a></center>
    <center>©CopyRight 2017, 武汉大学国际软件学院LTalk团队.</a></center>
</body>
</html>
package com.se2017.ltalk.socket.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.se2017.ltalk.socket.RequestHandler;

public class RequestExecutor {
	private static ExecutorService exec=Executors.newCachedThreadPool();
	private RequestExecutor(){
		throw new IllegalArgumentException();
	}
	public static void execute(RequestHandler handler){
		exec.execute(handler);
	}
}

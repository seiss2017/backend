package com.se2017.ltalk.socket;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

import com.se2017.ltalk.AI.AI;
import com.se2017.ltalk.dao.MessageMapper;
import com.se2017.ltalk.dao.NotificationMapper;
import com.se2017.ltalk.entity.Message;
import com.se2017.ltalk.service.UserService;
import com.se2017.ltalk.socket.util.ConversationUtil;
import com.se2017.ltalk.socket.util.Dispatcher;
import com.se2017.ltalk.socket.util.MessageConverter;
import com.se2017.ltalk.socket.util.RequestExecutor;
import com.se2017.ltalk.util.TokenUtil;
import com.se2017.ltalk.util.response.StatusCode;

import static com.se2017.ltalk.socket.util.ConversationUtil.Conversation.Status.*;
import static com.se2017.ltalk.socket.SocketResponse.*;

import lombok.val;
import lombok.extern.log4j.Log4j;

/**
 * 
 * 处理器
 * @author wangle
 *
 */
@Component
@Log4j
public class SocketHandler implements WebSocketHandler {
	@Autowired
	private UserService userService;
	@Autowired
	private MessageMapper messageMapper;
	@Autowired
	private NotificationMapper notificationMapper;
	//连接建立时
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		val attrs=session.getAttributes();
		val token=(String)attrs.get("token");
		val user=TokenUtil.extract(token);
		if(!userService.exists(user.getEmail())){
			Dispatcher.send(session,new SocketResponse(false,REQUEST_STATUS,StatusCode.USER_NULL,"用户不存在"));
			session.close();
		}
		else{
			Dispatcher.add(session,SessionEntity.builder()
					.userId(user.getId())
					.token(token)
					.email(user.getEmail())
					.build());
			Dispatcher.send(session,new SocketResponse(true,REQUEST_STATUS,StatusCode.SUCCESS,"连接成功"));
			//发送未读的评论消息
			notificationMapper.selectUnreadComment(user.getId())
			.stream()
			.peek(notification->notificationMapper.read(notification.getId()))
			.forEach(notification->Dispatcher.send(session,notification.getContent()));
			//发送未读的点赞消息
			notificationMapper.selectUnreadLike(user.getId())
			.stream()
			.peek(like->notificationMapper.read(like.getId()))
			.forEach(like->Dispatcher.send(session,like.getContent()));
		}
	}
	//收到消息时
	@Override
	public void handleMessage(WebSocketSession session, WebSocketMessage<?> msg) throws Exception {
		val message=msg.getPayload();
		if(!(message instanceof String)){
			Dispatcher.send(session, new SocketResponse(false,REQUEST_STATUS,StatusCode.ERROR_DATA,"数据格式错误"));
			return;
		}
		val request=MessageConverter.jsonToRequest((String)message);
		if(Objects.isNull(request)||request.getType()<=0||request.getSessionId()<0){
			Dispatcher.send(session, new SocketResponse(false,REQUEST_STATUS,StatusCode.ERROR_DATA,"数据格式错误"));
			return;
		}
		val entity=Dispatcher.get(session);
		val conversation=ConversationUtil.get(request.getSessionId());
		if(Objects.isNull(conversation)){
			Dispatcher.send(session, new SocketResponse(false,REQUEST_STATUS,StatusCode.SESSION_NOT_EXIST,"会话不存在"));
			return;
		}
		int current=entity.getUserId();
		int sponsor=conversation.getSponsor();
		int recipient=conversation.getRecipient();
		if(current!=sponsor&&current!=recipient){
			Dispatcher.send(session, new SocketResponse(false,REQUEST_STATUS,StatusCode.FAILED,"会话信息不一致"));
			return;
		}
		val status=ConversationUtil.getStatus(request.getSessionId());
		switch (request.getType()) {
		case SocketRequest.CHAT:
			if(!status.equals(CHATTING)){
				Dispatcher.send(session, new SocketResponse(false,REQUEST_STATUS,StatusCode.SESSION_STATUS_ERROR,"会话状态不一致"));
				return;
			}
			chat(session,request);
			break;
		case SocketRequest.ACCEPT:
			if(!status.equals(WAITING)){
				Dispatcher.send(session, new SocketResponse(false,REQUEST_STATUS,StatusCode.SESSION_STATUS_ERROR,"会话状态不一致"));
				return;
			}
			accept(session,request);
			break;
		case SocketRequest.CONTINUE:
			if(!status.equals(EXTEND)){
				Dispatcher.send(session, new SocketResponse(false,REQUEST_STATUS,StatusCode.SESSION_STATUS_ERROR,"会话状态不一致"));
				return;
			}
			plusOneSecond(session,request);
			break;
		case SocketRequest.STOP:
			if(!status.equals(CHATTING)){
				Dispatcher.send(session, new SocketResponse(false,REQUEST_STATUS,StatusCode.SESSION_STATUS_ERROR,"会话状态不一致"));
				return;
			}
			stop(session,request);
			break;
		case SocketRequest.START:
			if(!status.equals(MATCHED)){
				Dispatcher.send(session, new SocketResponse(false,REQUEST_STATUS,StatusCode.SESSION_STATUS_ERROR,"会话状态不一致"));
				return;
			}
			start(session,request);
			break;
		default:
			Dispatcher.send(session, new SocketResponse(false,REQUEST_STATUS,StatusCode.ERROR_DATA,"数据格式错误"));
			break;
		}
	}
	//出现错误时
	@Override
	public void handleTransportError(WebSocketSession session, Throwable e) throws Exception {
		log.error("连接出现错误:"+session.getAttributes(), e);
		e.printStackTrace();
		if(session.isOpen()){
			Dispatcher.send(session,new SocketResponse(false,REQUEST_STATUS,StatusCode.FAILED,"出现错误，连接关闭"));
			Dispatcher.remove(session);
			session.close();
		}
	}
	//连接关闭时
	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		Dispatcher.remove(session);
	}
	//是否支持分为多个部分发送的消息
	@Override
	public boolean supportsPartialMessages() {
		return false;
	}
	/**
	 * 聊天
	 * @param request
	 */
	public void chat(WebSocketSession session, SocketRequest request){
		if(request.getContent()==null){
			Dispatcher.send(session, new SocketResponse(false,REQUEST_STATUS,StatusCode.ERROR_DATA,"数据格式错误"));
			return;
		}
		val entity=Dispatcher.get(session);
		val conversation=ConversationUtil.get(request.getSessionId());
		int recevier=conversation.getSponsor()==entity.getUserId()?conversation.getRecipient():conversation.getSponsor();
		if(recevier==0){
			String answer=null;
			try{
				answer=AI.xiaoi(String.valueOf(entity.getUserId()), request.getContent());
			}
			catch(Throwable t){
				Dispatcher.send(session,new SocketResponse(false,REQUEST_STATUS,StatusCode.FAILED,"发送失败"));
				return;
			}
			Dispatcher.send(session,new SocketResponse(true,REQUEST_STATUS,StatusCode.SUCCESS,"发送成功"));
			if(answer.matches("[?| ]*")){
				answer="对不起，小浪机器人出现了一些问题，请重试";
			}
			val send=Message.builder().content(request.getContent())
					.from(entity.getUserId())
					.to(recevier)
					.build();
			val to=Message.builder().content(answer)
					.to(entity.getUserId())
					.from(recevier)
					.build();
			messageMapper.insert(send);
			messageMapper.insert(to);

			val data=new LinkedHashMap<String,Object>();
			data.put("from", userService.getInfo(Dispatcher.getLittleLang(recevier)));
			data.put("content", answer);
			data.put("sendTime", LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
			Dispatcher.send(session,new SocketResponse(true,CHAT,StatusCode.SUCCESS,data));
		}
		else if(!userService.exists(recevier)){
			Dispatcher.send(session,new SocketResponse(false,REQUEST_STATUS,StatusCode.USER_NULL,"用户不存在"));
		}
		else{
			RequestExecutor.execute(new RequestHandler(request){
				@Override
				public void handle(SocketRequest request) {
					val entity=Dispatcher.get(session);
					val msg=Message.builder().content(request.getContent())
							.from(entity.getUserId())
							.to(recevier)
							.build();
					if(Dispatcher.isOnline(recevier)){
						Map<String,Object> data=new LinkedHashMap<>();
						data.put("from", userService.info(entity.getUserId()));
						data.put("content", msg.getContent());
						data.put("sendTime", LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
						Dispatcher.send(recevier, new SocketResponse(true,CHAT,StatusCode.SUCCESS,data));
						Dispatcher.send(session,new SocketResponse(true,REQUEST_STATUS,StatusCode.SUCCESS,"发送成功"));
					}
					else{
						Dispatcher.send(session, new SocketResponse(false,REQUEST_STATUS,StatusCode.FAILED,"对方已离线"));
					}
					messageMapper.insert(msg);
				}
			});
		}
	}
	/**
	 * 发起会话请求
	 * @param session
	 * @param request
	 */
	public void start(WebSocketSession session, SocketRequest request){
		val conversation=ConversationUtil.get(request.getSessionId());
		val entity=Dispatcher.get(session);
		if(entity.getUserId()==conversation.getSponsor()){
			val sponsor=new HashMap<>();
			sponsor.put("sessionId", conversation.getId());
			sponsor.put("user", userService.info(conversation.getSponsor()));
			Dispatcher.send(conversation.getRecipient(), 
					new SocketResponse(true, SocketResponse.CONVERSATION_CONFIRM, 
							StatusCode.SUCCESS, sponsor));
			Dispatcher.send(session,new SocketResponse(true,REQUEST_STATUS,StatusCode.SUCCESS,"发送成功"));
			ConversationUtil.start(conversation.getId());
		}
		else{
			Dispatcher.send(conversation.getRecipient(), 
					new SocketResponse(false, SocketResponse.REQUEST_STATUS, 
							StatusCode.SESSION_STATUS_ERROR, "会话状态不一致"));
		}
	}
	/**
	 * 接受
	 * @param request
	 */
	public void accept(WebSocketSession session, SocketRequest request){
		ConversationUtil.accept(request.getSessionId());
		Dispatcher.send(session, new SocketResponse(true,REQUEST_STATUS,StatusCode.SUCCESS,"确认请求已发送"));
	}
	/**
	 * 续命
	 * @param request
	 */
	public void plusOneSecond(WebSocketSession session, SocketRequest request){
		val entity=Dispatcher.get(session);
		val conversation=ConversationUtil.get(request.getSessionId());
		if(entity.getUserId()==conversation.getSponsor()){
			ConversationUtil.sponsorConfirm(request.getSessionId());
		}
		else{
			ConversationUtil.recipientConfirm(request.getSessionId());
		}
		Dispatcher.send(session, new SocketResponse(true,REQUEST_STATUS,StatusCode.SUCCESS,"确认请求已发送"));
	}
	/**
	 * 强制停止会话
	 * @param request
	 */
	public void stop(WebSocketSession session, SocketRequest request){
		Dispatcher.send(session, new SocketResponse(true,REQUEST_STATUS,StatusCode.SUCCESS,"结束请求已发送"));
		ConversationUtil.interrupt(request.getSessionId());
	}
}

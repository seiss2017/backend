package com.se2017.ltalk.socket;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class SocketResponse {
	private boolean result;
	/**
	 * 消息类型
	 * 1请求状态
	 * 2私聊，
	 * 3评论通知
	 * 4点赞通知
	 * 5会话确认
	 * 6续命确认
	 */
	private int type;
	private int statusCode;
	private Object data;
	
	public static final int REQUEST_STATUS=1;
	public static final int CHAT=2;
	public static final int COMMENT_NOTIFICATION=3;
	public static final int LIKE_NOTIFICATION=4;
	public static final int CONVERSATION_CONFIRM=5;
	public static final int PLUS_ONE_SECOND=6;
}

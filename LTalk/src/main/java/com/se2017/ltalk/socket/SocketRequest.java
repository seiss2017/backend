package com.se2017.ltalk.socket;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * websocket请求
 * @author w-angler
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SocketRequest {
	/**
	 * 请求类型
	 * 1聊天请求
	 * 2接受对方的会话请求
	 * 3续命请求，用于时间到了续命
	 * 4强制停止会话
	 * 5发起聊天
	 */
	private int type;
	private int sessionId;
	private String content;
	
	public static final int CHAT=1;
	public static final int ACCEPT=2;
	public static final int CONTINUE=3;
	public static final int STOP=4;
	public static final int START=5;
}

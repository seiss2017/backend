package com.se2017.ltalk.socket.util;

import java.util.Objects;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import com.se2017.ltalk.entity.Session;
import com.se2017.ltalk.socket.SocketResponse;
import com.se2017.ltalk.socket.util.ConversationUtil.Conversation.Status;
import com.se2017.ltalk.util.response.StatusCode;

import lombok.val;

/**
 * 会话池
 * @author w-angler
 *
 */
public class ConversationUtil {
	private static ConcurrentHashMap<Integer,Conversation> sessionPool=new ConcurrentHashMap<>(10);
	private static Timer timer=new Timer();
	/**
	 * 由于有延时，均延迟10秒
	 */
	private static final int WAIT_TIME=10;//15;
	private static final int CHAT_TIME=310;//310;
	/**
	 * 添加到连接池
	 * @param sessionId
	 * @param entity
	 * @return
	 */
	public static void put(Integer sessionId,Session session){
		val conversation=getConversation(session.getSponsor());
		if(!Objects.isNull(conversation)){
			conversation.stoping();
		}
		val newConversation=new Conversation(session);
		if(session.getRecipient()==0){
			newConversation.waiting();
			newConversation.accept();
			newConversation.chatting();
			System.out.println("chat with lang");
		}
		sessionPool.put(sessionId,newConversation);
	}
	/**
	 * 获取实体
	 * @param session
	 * @return
	 */
	public static Session get(Integer sessionId){
		val conversation=sessionPool.get(sessionId);
		return Objects.isNull(conversation)?null:conversation.getSession();
	}
	/**
	 * 移出连接池
	 * @param session 会话
	 * @return
	 */
	public static Session remove(Integer sessionId){
		val conversation=sessionPool.remove(sessionId);
		return Objects.isNull(conversation)?null:conversation.getSession();
	}
	/**
	 * 是否正在对话
	 * @param userId
	 * @return
	 */
	public static boolean exists(int userId){
		for(val session:sessionPool.values()){
			if(session.getSession().getRecipient()==userId||session.getSession().getSponsor()==userId){
				return true;
			}
		}
		return false;
	}
	/**
	 * 获取用户处在的对话
	 * @param userId
	 * @return
	 */
	public static Conversation getConversation(int userId){
		for(val session:sessionPool.values()){
			if(session.getSession().getRecipient()==userId||session.getSession().getSponsor()==userId){
				return session;
			}
		}
		return null;
	}
	/**
	 * 获取会话的状态
	 * @param sessionId
	 * @return
	 */
	public static Status getStatus(int sessionId){
		return sessionPool.get(sessionId).getStatus();
	}
	public static void start(int sessionId){
		sessionPool.get(sessionId).waiting();
	}
	/**
	 * 确认对话
	 * @param sessionId
	 */
	public static void accept(int sessionId){
		sessionPool.get(sessionId).accept();
	}
	/**
	 * 确认对话
	 * @param sessionId
	 */
	public static void sponsorConfirm(int sessionId){
		sessionPool.get(sessionId).sponsorConfirm();
	}
	/**
	 * 确认对话
	 * @param sessionId
	 */
	public static void recipientConfirm(int sessionId){
		sessionPool.get(sessionId).recipientConfirm();
	}
	/**
	 * 强制中断会话
	 * @param sessionId
	 */
	public static void interrupt(int sessionId){
		val session=sessionPool.get(sessionId);
		session.stoping();
		Dispatcher.send(session.getSession().getSponsor(), 
				new SocketResponse(true, SocketResponse.REQUEST_STATUS, StatusCode.SORRY_YOU_ARE_GOOD_GUY, "聊天中断"));
		Dispatcher.send(session.getSession().getRecipient(), 
				new SocketResponse(true, SocketResponse.REQUEST_STATUS, StatusCode.SORRY_YOU_ARE_GOOD_GUY, "聊天中断"));
	}

	/**************************************************/

	/**
	 * 会话状态
	 * @author w-angler
	 *
	 */
	public static class Conversation{
		private Session session;
		/**
		 * 状态枚举
		 * @author w-angler
		 *
		 */
		public enum Status{
			STOP,//开始or结束
			MATCHED,//匹配到用户
			WAITING,//等待对方确认
			CHATTING,//聊天
			EXTEND;//等待续命
		}
		/**
		 * 状态值
		 */
		private volatile Status status;
		/**
		 * 是否对方已被确认
		 */
		private volatile boolean accepted;
		private volatile boolean recipientConfirmed;
		private volatile boolean sponsorConfirmed;
		public Conversation(Session session){
			this.session=session;
			status=Status.MATCHED;
		}
		public Session getSession(){
			return session;
		}
		public Status getStatus(){
			return status;
		}
		public void setStatus(Status status){
			this.status=status;
		}
		public int getSessionId(){
			return session.getId();
		}
		/**
		 * 确认聊天
		 */
		public void accept(){
			accepted=true;
		}
		/**
		 * 接收人确认
		 */
		public void recipientConfirm(){
			recipientConfirmed=true;
		}
		/**
		 * 发起者确认
		 */
		public void sponsorConfirm(){
			sponsorConfirmed=true;
		}
		/**
		 * 转为等待状态
		 */
		public void waiting(){
			status=Status.WAITING;
			timer.schedule(new ConfirmTask(this), WAIT_TIME*1000);
		}
		/**
		 * 转为聊天状态
		 */
		public void chatting(){
			status=Status.CHATTING;
			timer.schedule(new ChatTask(this), CHAT_TIME*1000);
		}
		/**
		 * 转为等待续命状态
		 */
		public void extending(){
			status=Status.EXTEND;
			timer.schedule(new ConfirmTask(this), WAIT_TIME*1000);
		}
		/**
		 * 转为停止状态
		 */
		public void stoping(){
			status=Status.STOP;
			sessionPool.remove(session.getId());
		}
	}
	/**
	 * 等待确认的定时任务
	 * @author w-angler
	 *
	 */
	static class ConfirmTask extends TimerTask{
		private Conversation conversation;
		public ConfirmTask(Conversation conversation) {
			this.conversation=conversation;
		}
		@Override
		public void run() {
			switch (conversation.getStatus()) {
			case WAITING:{
				if(conversation.accepted){
					conversation.accepted=false;
					Dispatcher.send(conversation.getSession().getSponsor(), 
							new SocketResponse(true, SocketResponse.REQUEST_STATUS, StatusCode.SUCCESS, "对方已同意聊天"));
					conversation.chatting();
					System.out.println("chatting");
				}
				else{
					Dispatcher.send(conversation.getSession().getSponsor(), 
							new SocketResponse(true, SocketResponse.REQUEST_STATUS, StatusCode.SORRY_YOU_ARE_GOOD_GUY, "对方已拒绝聊天"));
					conversation.stoping();
					System.out.println("stoping");
				}
				break;
			}
			case EXTEND:{
				if(conversation.recipientConfirmed&&conversation.sponsorConfirmed){
					conversation.recipientConfirmed=false;
					conversation.sponsorConfirmed=false;
					Dispatcher.send(conversation.getSession().getSponsor(), 
							new SocketResponse(true, SocketResponse.REQUEST_STATUS, StatusCode.SUCCESS, "对方已同意继续聊天"));
					Dispatcher.send(conversation.getSession().getRecipient(), 
							new SocketResponse(true, SocketResponse.REQUEST_STATUS, StatusCode.SUCCESS, "对方已同意继续聊天"));
					conversation.chatting();
					System.out.println("chatting");
				}
				else{
					Dispatcher.send(conversation.getSession().getSponsor(), 
							new SocketResponse(true, SocketResponse.REQUEST_STATUS, StatusCode.SORRY_YOU_ARE_GOOD_GUY, "已拒绝继续聊天"));
					Dispatcher.send(conversation.getSession().getRecipient(), 
							new SocketResponse(true, SocketResponse.REQUEST_STATUS, StatusCode.SORRY_YOU_ARE_GOOD_GUY, "已拒绝继续聊天"));
					conversation.stoping();
					System.out.println("stoping");
				}
				break;
			}
			default:
				break;
			}
		}
	}
	/**
	 * 聊天的定时任务
	 * @author w-angler
	 *
	 */
	static class ChatTask extends TimerTask{
		private Conversation conversation;
		public ChatTask(Conversation conversation) {
			this.conversation=conversation;
		}
		@Override
		public void run() {
			if(!conversation.getStatus().equals(Status.STOP)){
				if(conversation.getSession().getRecipient()!=0){
					Dispatcher.send(conversation.getSession().getSponsor(), 
							new SocketResponse(true, SocketResponse.PLUS_ONE_SECOND, StatusCode.SUCCESS, conversation.getSessionId()));
					Dispatcher.send(conversation.getSession().getRecipient(), 
							new SocketResponse(true, SocketResponse.PLUS_ONE_SECOND, StatusCode.SUCCESS, conversation.getSessionId()));
					conversation.extending();
					System.out.println("extending");
				}
				else{
					Dispatcher.send(conversation.getSession().getSponsor(), 
							new SocketResponse(true, SocketResponse.PLUS_ONE_SECOND, StatusCode.FAILED, conversation.getSessionId()));
				}
			}
		}
	}

	public static void main(String[] args) {
		val conversation=new Conversation(null);
		conversation.waiting();
		val in=new Scanner(System.in);
		while(conversation.getStatus()!=Status.STOP){
			if(in.nextBoolean()){
				conversation.accept();
			}
			if(in.nextBoolean()){
				conversation.recipientConfirm();
			}
			if(in.nextBoolean()){
				conversation.sponsorConfirm();
			}
		}
		in.close();
	}
}

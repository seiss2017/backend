package com.se2017.ltalk.socket;

public abstract class RequestHandler implements Runnable{
	private SocketRequest request;
	public RequestHandler(SocketRequest request){
		this.request=request;
	}
	public abstract void handle(SocketRequest request);
	@Override
	public void run() {
		handle(this.request);
	}
}

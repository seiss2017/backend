package com.se2017.ltalk.socket.config;

import java.util.Map;
import java.util.Objects;

import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import com.se2017.ltalk.interceptor.StatelessAuthcFilter;

import lombok.val;

/**
 * Http握手拦截器
 * @author w_angler
 *
 */

@Component
public class WebsocketHandshakeInterceptor implements HandshakeInterceptor {
	private static StatelessAuthcFilter filter=new StatelessAuthcFilter();
	/**
	 * websocket连接建立前的握手，验证参数合法性
	 */
	@Override
	public boolean beforeHandshake(ServerHttpRequest request,
			ServerHttpResponse response, WebSocketHandler handler,
			Map<String, Object> attrs) throws Exception {
		val httpRequest=((ServletServerHttpRequest)request).getServletRequest();
		val httpResponse=((ServletServerHttpResponse)response).getServletResponse();
		val token = httpRequest.getParameter("token");
		if(Objects.isNull(token)||(!filter.websocketFilter(httpRequest, httpResponse))){
			return false;
		}
		attrs.put("token", token);
		return true;
	}
	@Override
	public void afterHandshake(ServerHttpRequest request,
			ServerHttpResponse response, WebSocketHandler wsHandler,
			Exception exception) {
	}
}

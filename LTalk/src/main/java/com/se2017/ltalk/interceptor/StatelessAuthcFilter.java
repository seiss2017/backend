package com.se2017.ltalk.interceptor;

import java.util.Objects;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.web.filter.AccessControlFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.se2017.ltalk.util.response.Status;

import lombok.val;

public class StatelessAuthcFilter extends AccessControlFilter {
	private static ObjectMapper mapper = new ObjectMapper();

	@Override
	protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue)
			throws Exception {
		return false;
	}

	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
		val token = request.getParameter("token");
		try {
			val upt = new StatelessToken(Objects.isNull(token)?"":token);
			getSubject(request, response).login(upt);
		} catch (Exception e) {
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json");
			response.getWriter().write(mapper.writeValueAsString(Status.AUTHENTICATION_FAILED));
			return false;
		}
		return true;
	}
	/**
	 * websocket的token验证
	 * @param request
	 * @param response
	 * @return
	 */
	public boolean websocketFilter(ServletRequest request, ServletResponse response){
		val token = request.getParameter("token");
		try {
			val upt = new StatelessToken(token);
			getSubject(request, response).login(upt);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}

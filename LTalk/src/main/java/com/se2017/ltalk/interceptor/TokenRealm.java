package com.se2017.ltalk.interceptor;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import com.se2017.ltalk.entity.User;
import com.se2017.ltalk.service.UserService;

import lombok.val;

public class TokenRealm extends AuthorizingRealm{
	@Autowired
	private UserService userService;
	
    @Override
    public boolean supports(AuthenticationToken token) {
        return (token instanceof StatelessToken)||(token instanceof UsernamePasswordToken);
    }
	
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		return new SimpleAuthorizationInfo();
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		if(token instanceof StatelessToken){
			val user =token.getPrincipal();
			if(user!=null&&userService.validate((User)user)){
				return new SimpleAuthenticationInfo(user,user,getName());
			}
		}
		throw new AuthenticationException();
	}

}

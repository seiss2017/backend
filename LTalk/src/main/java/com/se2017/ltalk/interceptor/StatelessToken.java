package com.se2017.ltalk.interceptor;

import org.apache.shiro.authc.AuthenticationToken;

import com.se2017.ltalk.entity.User;
import com.se2017.ltalk.util.TokenUtil;

public class StatelessToken implements AuthenticationToken {
	private static final long serialVersionUID = 1L;
	User user;

	public StatelessToken(String token){
		user=TokenUtil.extract(token);
	}
	
	@Override
	public Object getPrincipal() {
		return user;
	}
	@Override
	public Object getCredentials() {
		return user;
	}

}

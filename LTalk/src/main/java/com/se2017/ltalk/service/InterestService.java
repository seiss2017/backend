package com.se2017.ltalk.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.se2017.ltalk.dao.InterestMapper;
import com.se2017.ltalk.dao.InterestsMapper;
import com.se2017.ltalk.entity.Interest;
import com.se2017.ltalk.entity.Interests;

import lombok.val;

@Service
public class InterestService {
	@Autowired
	private InterestMapper interestMapper;
	@Autowired
	private InterestsMapper interestsMapper;
	/**
	 * 获取所有的类型
	 * @return
	 */
	public List<String> type(){
		return interestMapper.selectAllTypes().stream().map(i->i.getType()).distinct().collect(Collectors.toList());
	}
	/**
	 * 获取某个类型下的兴趣
	 * @param name
	 * @return
	 */
	public List<Interest> type(String name){
		return interestMapper.selectByType(name);
	}
	/**
	 * 获取所有的信息列表
	 * @return
	 */
	public List<Interest> all(){
		return interestMapper.selectAll();
	}
	/**
	 * 获取用户的兴趣列表
	 * @param userId
	 * @return
	 */
	public List<Interest> get(int userId){
		return interestsMapper.selectByUserId(userId).stream().map(e->interestMapper.selectByPrimaryKey(e.getInterestId())).collect(Collectors.toList());
	}
	/**
	 * 添加用户的兴趣列表
	 * @param userId
	 * @return
	 */
	public void add(int userId,int[] ids){
		val all=interestsMapper.selectByUserId(userId).stream().map(e->e.getInterestId()).distinct().collect(Collectors.toSet());
		val total=interestMapper.selectAll().stream().map(e->e.getId()).distinct().collect(Collectors.toSet());
		for(int id:ids){
			if(!all.contains(id)&&total.contains(id)){
				interestsMapper.insert(Interests.builder().interestId(id).userId(userId).build());
			}
		}
	}
	/**
	 * 删除用户的兴趣列表
	 * @param userId
	 * @return
	 */
	public boolean delete(int userId,int[] ids){
		interestsMapper.selectByUserId(userId).forEach(i->{
			for(int id:ids){
				if(id==i.getInterestId()){
					interestsMapper.deleteByPrimaryKey(i.getId());
				}
			}
		});
		return true;
	}
	/**
	 * 获取用户的特征向量
	 * @param userIds
	 * @return
	 */
	public List<int[]> getFeatures(int[] userIds){
		val features=new ArrayList<int[]>(userIds.length);
		val interests=interestMapper.selectAll();
		Arrays.stream(userIds).forEach(useId->{
			val userInterests=get(useId);
			features.add(interests.stream().mapToInt(interest->userInterests.contains(interest)?1:0).toArray());
		});
		return features;
	}
}

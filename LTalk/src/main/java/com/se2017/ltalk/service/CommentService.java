package com.se2017.ltalk.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.se2017.ltalk.dao.CommentMapper;
import com.se2017.ltalk.dao.PiazzaMapper;
import com.se2017.ltalk.entity.Comment;
import com.se2017.ltalk.util.pagination.Page;
import com.se2017.ltalk.util.pagination.PageAdapter;

import lombok.val;

@Service
public class CommentService {
	@Autowired
	private CommentMapper commentMapper;
	@Autowired
	private PiazzaMapper piazzaMapper;
	@Autowired
	private UserService userService;
	@Autowired
	private PiazzaService piazzaService;
	@Autowired
	private LikeService likeService;
	@Autowired
	private PushService pushService;
	/**
	 * 获取评论
	 * @param piazzaId
	 * @param page
	 * @param rows
	 * @return
	 */
	public Page get(int userId,int piazzaId,int page,int rows){
		if(!piazzaService.exists(piazzaId)){
			return null;
		}
		val piazzas=new ArrayList<Map<String,Object>>();
		PageHelper.startPage(page, rows);
		val list=commentMapper.selectByPiazzaId(piazzaId);
		val info=new PageInfo<Comment>(list);
		list.forEach((e)->{
			piazzas.add(this.buildMap(userId,e));
		});
		return PageAdapter.adapt(info, piazzas);
	}
	/**
	 * 添加
	 * @param reviewer
	 * @param content
	 * @return
	 */
	public Map<String,Object> add(int userId,int piazzaId,int reviewer,String content,int pointTo,int commentId){
		val piazza=piazzaMapper.selectByPrimaryKey(piazzaId);
		if(Objects.isNull(piazza)){
			return null;
		}
		val comment=Comment.builder().piazzaId(piazzaId).content(content).createTime(LocalDateTime.now()).reviewer(reviewer).pointTo(pointTo).build();
		commentMapper.insert(comment);
		val map=buildMap(pointTo, comment);
		if(pointTo!=0){
			map.put("source", buildMap(pointTo,commentMapper.selectByPrimaryKey(commentId)));
			pushService.pushComment(pointTo, map);
		}
		else{
			map.put("source", piazzaService.buildMap(piazza.getPoster(), piazza));
			pushService.pushComment(piazza.getPoster(), map);
		}
		return buildMap(userId,comment);
	}
	/**
	 * 是否存在该评论
	 * @param commentId
	 * @return
	 */
	public boolean exists(int commentId){
		return !Objects.isNull(commentMapper.selectByPrimaryKey(commentId));
	}
	/**
	 * 点赞
	 * @param piazzaId
	 * @param commentId
	 * @return
	 */
	public Object like(int userId,int piazzaId,int commentId){
		val comment=commentMapper.selectByPrimaryKey(commentId);
		if(Objects.isNull(comment)||comment.getPiazzaId()!=piazzaId){
			return null;
		}
		if(likeService.likeComment(userId, commentId)){
			comment.setLikes(comment.getLikes()+1);
			commentMapper.like(comment);
			pushService.pushLike(comment.getReviewer(), buildMap(comment.getReviewer(), comment));
		}
		return comment.getLikes();
	}
	/**
	 * 取消点赞
	 * @param piazzaId
	 * @param commentId
	 * @return
	 */
	public Object dislike(int userId,int piazzaId,int commentId){
		val comment=commentMapper.selectByPrimaryKey(commentId);
		if(Objects.isNull(comment)||comment.getPiazzaId()!=piazzaId){
			return null;
		}
		if(likeService.dislikeComment(userId, commentId)){
			comment.setLikes(comment.getLikes()-1);
			commentMapper.like(comment);
		}
		return comment.getLikes();
	}
	
	public Map<String,Object> buildMap(int userId,Comment comment){
		val map=new HashMap<String,Object>();
		map.put("id", comment.getId());
		map.put("piazzaId", comment.getPiazzaId());
		map.put("likes", comment.getLikes());
		map.put("hasLiked", likeService.hasLikeComment(userId, comment.getId()));
		map.put("content", comment.getContent());
		map.put("time", comment.getCreateTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		map.put("reviewer", userService.info(comment.getReviewer()));
		if(comment.getPointTo()!=0){
			map.put("pointTo", userService.info(comment.getPointTo()));
		}
		return map;
	}
}

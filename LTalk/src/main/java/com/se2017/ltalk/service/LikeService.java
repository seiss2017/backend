package com.se2017.ltalk.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.se2017.ltalk.dao.CommentLikeMapper;
import com.se2017.ltalk.dao.PiazzaLikeMapper;
import com.se2017.ltalk.entity.CommentLike;
import com.se2017.ltalk.entity.PiazzaLike;

import lombok.val;

@Service
public class LikeService {
	@Autowired
	private CommentLikeMapper commentLikeMapper;
	@Autowired
	private PiazzaLikeMapper piazzaLikeMapper;
	
	/**
	 * 给动态点赞
	 * @param userId
	 * @param piazzaId
	 * @return
	 */
	public boolean likePiazza(int userId,int piazzaId){
		val like=PiazzaLike.builder().userId(userId).piazzaId(piazzaId).build();
		PiazzaLike exists=null;
		if((exists=piazzaLikeMapper.select(like))==null){
			piazzaLikeMapper.insert(like);
			return true;
		}
		if(exists.isDeleted()){
			piazzaLikeMapper.disdelete(exists);
			return true;
		}
		return false;
	}
	/**
	 * 给评论点赞
	 * @param userId
	 * @param commentId
	 * @return
	 */
	public boolean likeComment(int userId,int commentId){
		val like=CommentLike.builder().userId(userId).commentId(commentId).build();
		CommentLike exists=null;
		if((exists=commentLikeMapper.select(like))==null){
			commentLikeMapper.insert(like);
			return true;
		}
		if(exists.isDeleted()){
			commentLikeMapper.disdelete(exists);
			return true;
		}
		return false;
	}
	/**
	 * 取消动态点赞
	 * @param userId
	 * @param piazzaId
	 * @return
	 */
	public boolean dislikePiazza(int userId,int piazzaId){
		val like=PiazzaLike.builder().userId(userId).piazzaId(piazzaId).build();
		val result=piazzaLikeMapper.select(like);
		if(result==null){
			return false;
		}
		if(result.isDeleted()){
			return false;
		}
		piazzaLikeMapper.delete(like);
		return true;
	}
	/**
	 * 取消评论点赞
	 * @param userId
	 * @param commentId
	 * @return
	 */
	public boolean dislikeComment(int userId,int commentId){
		val like=CommentLike.builder().userId(userId).commentId(commentId).build();
		val result=commentLikeMapper.select(like);
		if(result==null){
			return false;
		}
		if(result.isDeleted()){
			return false;
		}
		commentLikeMapper.delete(like);
		return true;
	}
	/**
	 * 是否已经赞了动态
	 * @param userId
	 * @param piazzaId
	 * @return
	 */
	public boolean hasLikePiazza(int userId,int piazzaId){
		val like=PiazzaLike.builder().userId(userId).piazzaId(piazzaId).build();
		val result=piazzaLikeMapper.select(like);
		return !Objects.isNull(result)&&!result.isDeleted();
	}
	/**
	 * 是否已经赞了评论
	 * @param userId
	 * @param commentId
	 * @return
	 */
	public boolean hasLikeComment(int userId,int commentId){
		val like=CommentLike.builder().userId(userId).commentId(commentId).build();
		val result=commentLikeMapper.select(like);
		return !Objects.isNull(result)&&!result.isDeleted();
	}
}

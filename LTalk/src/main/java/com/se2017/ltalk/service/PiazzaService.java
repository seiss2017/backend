package com.se2017.ltalk.service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.se2017.ltalk.dao.CommentMapper;
import com.se2017.ltalk.dao.PiazzaMapper;
import com.se2017.ltalk.entity.Piazza;
import com.se2017.ltalk.util.encrypt.AES;
import com.se2017.ltalk.util.encrypt.MD5;
import com.se2017.ltalk.util.fileupload.ResourceManager;
import com.se2017.ltalk.util.pagination.Page;
import com.se2017.ltalk.util.pagination.PageAdapter;

import lombok.val;

@Service
public class PiazzaService {
	@Autowired
	private PiazzaMapper piazzaMapper;
	@Autowired
	private UserService userService;
	@Autowired
	private CommentMapper commentMapper;
	@Autowired
	private LikeService likeService;
	@Autowired
	private PushService pushService;
	/**
	 * 获取最新的广场信息，并且分页
	 * @param page
	 * @param rows
	 * @return
	 */
	public Page latest(int userId,Integer page,Integer rows) {
		val piazzas=new ArrayList<Map<String,Object>>();
		PageHelper.startPage(page, rows);
		val list=piazzaMapper.selectAll();
		val info=new PageInfo<Piazza>(list);
		list.forEach((e)->{
			piazzas.add(this.buildMap(userId,e));
		});
		return PageAdapter.adapt(info, piazzas);
	}
	/**
	 * 获取
	 * @param id
	 * @return
	 */
	public Map<String,Object> get(int userId, int id) {
		val piazza=piazzaMapper.selectByPrimaryKey(id);
		return Objects.isNull(piazza)?null:buildMap(userId,piazza);
	}
	/**
	 * 是否存在
	 * @param id
	 * @return
	 */
	public boolean exists(int id){
		return !Objects.isNull(piazzaMapper.selectByPrimaryKey(id));
	}
	/**
	 * 添加
	 * @param images 图片
	 * @param poster 发送者
	 * @param content 内容
	 * @param time
	 * @return
	 */
	public Map<String, Object> add(int userId,MultipartFile[] images,int poster,String content,long time){
		val paths=new ArrayList<String>();
		if(!Objects.isNull(images)&&images.length>0){
			for(int i=0;i<images.length;i++){
				try {
					paths.add(ResourceManager.savePiazzaImage(images[i], String.valueOf(poster)));
				} catch (IOException e) {
					return null;
				}
			}
		}
		val now=LocalDateTime.now();
		val expireTime=now.plusMinutes(time);
		val piazza=Piazza.builder().content(content).expireTime(expireTime).createTime(now).images(String.join(",",paths)).poster(poster).build();
		piazzaMapper.insert(piazza);
		return buildMap(userId,piazza);
	}
	/**
	 * 点赞
	 * @param piazzaId
	 * @return
	 */
	public Object like(int userId,int piazzaId){
		val piazza=piazzaMapper.selectByPrimaryKey(piazzaId);
		if(Objects.isNull(piazza)){
			return null;
		}
		if(likeService.likePiazza(userId, piazzaId)){
			piazza.setLikes(piazza.getLikes()+1);
			piazzaMapper.like(piazza);
			if(userId!=piazza.getPoster()){
				val like=buildMap(piazza.getPoster(), piazza);
				like.put("liker", userService.info(userId));
				pushService.pushLike(piazza.getPoster(), like);
			}
		}
		return piazza.getLikes();
	}
	/**
	 * 取消点赞
	 * @param piazzaId
	 * @return
	 */
	public Object dislike(int userId,int piazzaId){
		val piazza=piazzaMapper.selectByPrimaryKey(piazzaId);
		if(Objects.isNull(piazza)){
			return null;
		}
		if(likeService.dislikePiazza(userId, piazzaId)){
			piazza.setLikes(piazza.getLikes()-1);
			piazzaMapper.like(piazza);
		}
		return piazza.getLikes();
	}
	/**
	 * 获取分享链接
	 * @param piazzaId
	 * @return
	 */
	public String share(int userId,int piazzaId){
		val piazza=piazzaMapper.selectByPrimaryKey(piazzaId);
		if(Objects.isNull(piazza)||piazza.getPoster()!=userId){
			return null;
		}
		return ResourceManager.shareBase+AES.encrypt(MD5.hash(piazza.getContent()+piazza.getImages()+piazza.getPoster())+piazzaId, 
				AES.KEY);
	}
	/**
	 * 销毁
	 * @return
	 */
	public int destroy(){
		int sum=0;
		val now=LocalDateTime.now();
		for(val piazza:piazzaMapper.selectAllExist()){
			val deadline=piazza.getExpireTime();
			if(now.isAfter(deadline)){
				piazzaMapper.destroy(piazza.getId());
			}
		}
		return sum;
	}

	public Map<String,Object> buildMap(int userId,Piazza piazza){
		val map=new HashMap<String,Object>();
		map.put("id", piazza.getId());
		map.put("content", piazza.getContent());
		map.put("time", piazza.getCreateTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		map.put("poster", userService.info(piazza.getPoster()));
		map.put("images", piazza.getImages().split(","));
		map.put("likes", piazza.getLikes());
		map.put("hasLiked", likeService.hasLikePiazza(userId, piazza.getId()));
		map.put("comments", commentMapper.comments(piazza.getId()));
		return map;
	}
	public Map<String,Object> buildMap(Piazza piazza){
		val map=new HashMap<String,Object>();
		map.put("id", piazza.getId());
		map.put("content", piazza.getContent());
		map.put("time", piazza.getCreateTime()
				.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
				.replace("T", " "));
		map.put("poster", userService.info(piazza.getPoster()));
		map.put("images", piazza.getImages().split(","));
		map.put("likes", piazza.getLikes());
		map.put("comments", commentMapper.comments(piazza.getId()));
		return map;
	}

}

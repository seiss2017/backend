package com.se2017.ltalk.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.se2017.ltalk.dao.NotificationMapper;
import com.se2017.ltalk.entity.Notification;
import com.se2017.ltalk.socket.SocketResponse;
import com.se2017.ltalk.socket.util.Dispatcher;
import com.se2017.ltalk.socket.util.MessageConverter;
import com.se2017.ltalk.util.response.StatusCode;

import lombok.val;

/**
 * 推送服务
 * @author w-angler
 *
 */
@Service
public class PushService {
	@Autowired
	private NotificationMapper notificationMapper;
	
	/**
	 * 向用户推送评论数据
	 * @param userId
	 * @param data
	 */
	public void pushComment(int userId,Map<String,Object> data){
		val notification=Notification.builder()
				.content(MessageConverter.objectToJson(
						SocketResponse.builder()
						.data(data)
						.result(true)
						.statusCode(StatusCode.SUCCESS)
						.type(SocketResponse.COMMENT_NOTIFICATION)
						.build()
						))
				.type(SocketResponse.COMMENT_NOTIFICATION)
				.receiver(userId)
				.build();
		if(Dispatcher.send(userId, notification.getContent())){
			notification.setRead(true);
		}
		else{
			notification.setRead(false);
		}
		notificationMapper.insert(notification);
	}
	public void pushLike(int userId,Map<String,Object> data){
		val notification=Notification.builder()
				.content(MessageConverter.objectToJson(
						SocketResponse.builder()
						.data(data)
						.result(true)
						.statusCode(StatusCode.SUCCESS)
						.type(SocketResponse.LIKE_NOTIFICATION)
						.build()
						))
				.type(SocketResponse.LIKE_NOTIFICATION)
				.receiver(userId)
				.build();
		if(Dispatcher.send(userId, notification.getContent())){
			notification.setRead(true);
		}
		else{
			notification.setRead(false);
		}
		notificationMapper.insert(notification);
	}
}

package com.se2017.ltalk.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.se2017.ltalk.dao.InformMapper;
import com.se2017.ltalk.entity.Inform;

@Service
public class InformService {
	@Autowired
	private InformMapper informMapper;
	@Autowired
	private UserService userService;
	
	public boolean inform(int informer,int respondent,String reason){
		if(!userService.exists(respondent)){
			return false;
		}
		informMapper.insert(Inform.builder()
				.informer(informer)
				.respondent(respondent)
				.reason(reason)
				.build());
		return true;
	}

}

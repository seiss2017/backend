package com.se2017.ltalk.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.mail.MessagingException;

import org.springframework.stereotype.Service;

import com.se2017.ltalk.util.Email;
import com.se2017.ltalk.util.encrypt.AES;
import com.se2017.ltalk.util.encrypt.MD5;

import lombok.val;

@Service
public class CheckerService {
	private static SimpleDateFormat formatter=new SimpleDateFormat("yyMMddHHmmss");
	/**
	 * 发送验证码
	 * @param email
	 * @return
	 */
	public String sendChecker(String email){
		try {
			val time=formatter.format(new Date());
			return AES.encrypt(time+email, MD5.hash(Email.sendTo(email).toLowerCase()).substring(0, 16));
		} catch (MessagingException e) {
			return null;
		}
	}
	/**
	 * 验证验证码
	 * @param email
	 * @param cipher
	 * @param checker
	 * @return 0合法，1过期，2验证失败
	 */
	public int validate(String email,String cipher,String checker){
		val plaintext=AES.decrypt(cipher, MD5.hash(checker.toLowerCase()).substring(0,16));
		if(plaintext==null){
			return 2;
		}
		val time=plaintext.substring(0, 12);
		val reveiver=plaintext.substring(12, plaintext.length());
		try{
			val date=formatter.parse(time);
			val now=new Date();
			if(now.getTime()-date.getTime()>1000*60*5){
				return 1;
			}
			return email.equals(reveiver)?0:2;
		}
		catch(Exception e){
			return 2;
		}
	}
}

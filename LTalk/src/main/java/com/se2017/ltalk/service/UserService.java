package com.se2017.ltalk.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import org.apache.xmlrpc.XmlRpcException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.web.multipart.MultipartFile;

import com.se2017.ltalk.dao.SessionMapper;
import com.se2017.ltalk.dao.UserMapper;
import com.se2017.ltalk.entity.Session;
import com.se2017.ltalk.entity.User;
import com.se2017.ltalk.recommend.Recommendation;
import com.se2017.ltalk.socket.util.ConversationUtil;
import com.se2017.ltalk.socket.util.Dispatcher;
import com.se2017.ltalk.util.TokenUtil;
import com.se2017.ltalk.util.encrypt.MD5;
import com.se2017.ltalk.util.fileupload.ResourceManager;

import lombok.val;

@Service
public class UserService {
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private InterestService interestService;
	@Autowired
	private SessionMapper sessionMapper;
	/**
	 * 默认的头像地址
	 */
	private static final String defaultAvatar="https://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&f=y";

	/**
	 * 登录
	 * @param email
	 * @param passwd
	 * @return
	 */
	public String login(String email,String passwd){
		val user=userMapper.selectByEmail(email);
		if(user!=null&&user.getPasswd().equals(MD5.hash(passwd))){
			val token=TokenUtil.generate(user);
			user.setToken(token);
			userMapper.updateToken(user);
			return token;
		}
		return null;
	}
	/**
	 * 登出
	 * @param user
	 * @return
	 */
	public boolean logout(int userId){
		User user=userMapper.selectByPrimaryKey(userId);
		if(!Objects.isNull(user)){
			user.setToken(MD5.hash(UUID.randomUUID().toString()));
			userMapper.updateToken(user);
			return true;
		}
		return false;
	}
	/**
	 * 注册
	 * @param email
	 * @param passwd
	 * @return
	 */
	public synchronized String register(String email,String passwd,String checker){
		val user=User.builder()
				.avatar(defaultAvatar)
				.passwd(MD5.hash(passwd))
				.email(email)
				.nickname(email)
				.build();
		userMapper.insert(user);
		val token=TokenUtil.generate(user);
		user.setToken(token);
		userMapper.updatePasswd(user);
		return token;
	}
	/**
	 * 更换密码
	 * @param email
	 * @param passwd
	 * @return
	 */
	public String changePasswd(String email,String passwd,String old){
		val user=userMapper.selectByEmail(email);
		if(!Objects.isNull(old)){
			if(!user.getPasswd().equals(MD5.hash(old))){
				return null;
			}
		}
		val token=TokenUtil.generate(user);
		user.setPasswd(MD5.hash(passwd));
		userMapper.updatePasswd(user);
		return token;
	}
	/**
	 * 用户是否已存在
	 * @param email
	 * @return
	 */
	public boolean exists(String email){
		if(Objects.isNull(email)){
			return true;
		}
		return !Objects.isNull(userMapper.selectByEmail(email));
	}
	public boolean exists(int id){
		return !Objects.isNull(userMapper.selectByPrimaryKey(id));
	}
	/**
	 * 验证合法性
	 * @param user
	 * @return
	 */
	public boolean validate(User user){
		User u=null;
		return (u=userMapper.selectByEmail(user.getEmail()))!=null&&u.getToken().equals(user.getToken());
	}
	/**
	 * 获取用户的信息
	 * @param token
	 * @return
	 */
	public Map<String,Object> info(String email) {
		return getInfo(userMapper.selectByEmail(email));
	}
	public Map<String,Object> info(int id) {
		return getInfo(userMapper.selectByPrimaryKey(id));
	}
	public Map<String,Object> info(String email,String intro,String sex,String nickname) {
		val user=userMapper.selectByEmail(email);
		if(!"".equals(intro)){
			user.setIntro(intro);
		}
		if(!"".equals(sex)){
			user.setSex(sex.equals("g"));
		}
		if(!"".equals(nickname)){
			user.setNickname(nickname);
		}
		userMapper.updateByPrimaryKey(user);
		return getInfo(user);
	}
	/**
	 * 上传头像
	 * @param file
	 * @return
	 */
	public String uploadHead(MultipartFile  file,String email){
		String path=null;
		try {
			path=ResourceManager.saveHead(file, MD5.hash(email+System.currentTimeMillis()));
			val user=userMapper.selectByEmail(email);
			user.setAvatar(path);
			userMapper.updateByPrimaryKey(user);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return path;
	}

	/**
	 * 获取用户的信息
	 * @param user
	 * @return
	 */
	public Map<String,Object> getInfo(User user){
		val info=new HashMap<String,Object>();
		info.put("id", user.getId());
		info.put("avatar", user.getAvatar());
		info.put("email", user.getEmail());
		info.put("intro", user.getIntro());
		info.put("nickname", user.getNickname());
		info.put("sex", user.isSex()?"g":"m");
		return info;
	}
	/**
	 * 获取潜在的兴趣相同的朋友
	 * @param userId
	 * @return
	 * @throws XmlRpcException 
	 */
	public Map<String,Object> friends(int userId) throws XmlRpcException{
		//20%的概率触发小浪机器人
		if(Math.random()>0.2){
			//获取所有的在线用户
			val userIds=userMapper.selectAll().stream().mapToInt(user->user.getId()).filter(Dispatcher::isOnline).toArray();
			//如果在线的用户数大于1（有除了本身以外的其他用户）
			if(userIds.length>1){
				//匹配结果
				val result=Recommendation.match(interestService.getFeatures(userIds), userIds);
				//查找在线的用户
				for(int id:result.get(userId)){
					if(Dispatcher.isOnline(id)&&!ConversationUtil.exists(id)){
						if(userId==id){
							continue;
						}
						val session=Session.builder().sponsor(userId).recipient(id).lastTime(300).build();
						sessionMapper.insert(session);
						val info=info(id);
						val conversation=new HashMap<String,Object>();
						conversation.put("sessionId", session.getId());
						conversation.put("user", info);
						ConversationUtil.put(session.getId(),session);
						return conversation;
					}
				}
			}
		}
		val lang=Dispatcher.getLittleLang();
		val session=Session.builder().sponsor(userId).recipient(lang.getId()).lastTime(300).build();
		sessionMapper.insert(session);
		val info=getInfo(lang);
		val conversation=new HashMap<String,Object>();
		conversation.put("sessionId", session.getId());
		conversation.put("user", info);
		ConversationUtil.put(session.getId(),session);
		return conversation;
	}
}

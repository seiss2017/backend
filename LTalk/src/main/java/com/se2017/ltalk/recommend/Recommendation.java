package com.se2017.ltalk.recommend;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.junit.Test;

import lombok.val;

/**
 * 用户推荐
 * @author w-angler
 *
 */
public class Recommendation {
	/**
	 * RPC客户端
	 */
	private static final XmlRpcClient client = new XmlRpcClient();
	//初始化
	static{
		val config = new XmlRpcClientConfigImpl();
		try {
			config.setServerURL(new URL("http://localhost:8000"));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		client.setConfig(config);
	}
	/**
	 * 获取所有用户的推荐
	 * @param features 特征向量
	 * @param userId 索引对应的用户ID
	 * @return 每个用户的推荐列表
	 * @throws XmlRpcException
	 */
	public static Map<Integer, int[]> match(List<int[]> features,int[] userId) throws XmlRpcException{
		val result=new HashMap<Integer, int[]>(userId.length);
		val matrix=(Object[])client.execute("match", new Object[]{
				features.stream().map(e->{
					return IntStream.of(e).boxed().collect(Collectors.toList());
				}).collect(Collectors.toList())
		});
		int i=0;
		for(val res:matrix){
			val elements=(Object[])res;
			val users=new int[elements.length];
			for(int j=0,len=users.length;j<len;j++){
				users[j]=userId[(Integer)elements[j]];
			}
			result.put(userId[i++], users);
		}
		return result;
	}

	@Test
	public void test(){        
		try {
			@SuppressWarnings("serial")
			val result = match(new ArrayList<int[]>(){{
				add(new int[]{1,0,0,1,1,1,0,1,0});
				add(new int[]{1,0,1,0,1,1,1,0,0});
				add(new int[]{1,0,0,1,1,1,0,1,0});
				add(new int[]{1,1,0,0,0,1,1,1,0});
				add(new int[]{1,0,1,1,0,1,0,1,0});
				add(new int[]{1,0,1,1,1,0,0,1,0});
			}},new int[]{233,45,345,23,56,678});
			result.forEach((k,v)->{
				System.out.printf("%d:%s\n",k,Arrays.toString(v));
			});
		} catch (XmlRpcException e11) {
			e11.printStackTrace();
		}
	}
}

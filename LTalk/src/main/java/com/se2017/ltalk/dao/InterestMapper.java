package com.se2017.ltalk.dao;

import com.se2017.ltalk.entity.Interest;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface InterestMapper {
    int deleteByPrimaryKey(Integer id);
    int insert(Interest record);
    Interest selectByPrimaryKey(Integer id);
    List<Interest> selectAll();
    List<Interest> selectAllTypes();
    List<Interest> selectByType(String name);
    int updateByPrimaryKey(Interest record);
}
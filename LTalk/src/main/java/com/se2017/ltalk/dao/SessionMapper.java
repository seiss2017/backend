package com.se2017.ltalk.dao;

import com.se2017.ltalk.entity.Session;
import com.se2017.ltalk.entity.Today;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface SessionMapper {
    int deleteByPrimaryKey(Integer id);
    int insert(Session record);
    Session selectByPrimaryKey(Integer id);
    List<Session> selectAll();
    int updateByPrimaryKey(Session record);
    int selectToday(Today today);
}
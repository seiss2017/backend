package com.se2017.ltalk.dao;

import com.se2017.ltalk.entity.Comment;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface CommentMapper {
    int deleteByPrimaryKey(Integer id);
    int insert(Comment record);
    Comment selectByPrimaryKey(Integer id);
    List<Comment> selectAll();
    List<Comment> selectByPiazzaId(Integer piazzaId);
    int updateByPrimaryKey(Comment record);
    int like(Comment comment);
    int comments(int piazzaId);
}
package com.se2017.ltalk.dao;

import com.se2017.ltalk.entity.CommentLike;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentLikeMapper {
    int delete(CommentLike record);
    int disdelete(CommentLike record);
    int insert(CommentLike record);
    CommentLike select(CommentLike record);
}
package com.se2017.ltalk.dao;

import com.se2017.ltalk.entity.Inform;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface InformMapper {
    int deleteByPrimaryKey(Integer id);
    int insert(Inform record);
    Inform selectByPrimaryKey(Integer id);
    List<Inform> selectAll();
    int updateByPrimaryKey(Inform record);
}
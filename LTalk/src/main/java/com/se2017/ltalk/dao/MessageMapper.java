package com.se2017.ltalk.dao;

import com.se2017.ltalk.entity.Message;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface MessageMapper {
    int deleteByPrimaryKey(Integer id);
    int insert(Message record);
    Message selectByPrimaryKey(Integer id);
    List<Message> selectAll();
    int updateByPrimaryKey(Message record);
}
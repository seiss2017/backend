package com.se2017.ltalk.dao;

import com.se2017.ltalk.entity.PiazzaLike;
import org.springframework.stereotype.Repository;

@Repository
public interface PiazzaLikeMapper {
    int delete(PiazzaLike record);
    int disdelete(PiazzaLike record);
    int insert(PiazzaLike record);
    PiazzaLike select(PiazzaLike record);
}
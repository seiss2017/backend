package com.se2017.ltalk.dao;

import com.se2017.ltalk.entity.Interests;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface InterestsMapper {
    int deleteByPrimaryKey(Integer id);
    int insert(Interests record);
    Interests selectByPrimaryKey(Integer id);
    List<Interests> selectAll();
    List<Interests> selectByUserId(Integer id);
    int updateByPrimaryKey(Interests record);
}
package com.se2017.ltalk.dao;

import com.se2017.ltalk.entity.User;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {
    int deleteByPrimaryKey(Integer id);
    int insert(User record);
    User selectByPrimaryKey(Integer id);
    User selectByEmail(String email);
    List<User> selectAll();
    int updateByPrimaryKey(User record);
    int updateToken(User record);
    int updatePasswd(User record);
}
package com.se2017.ltalk.dao;

import com.se2017.ltalk.entity.Notification;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface NotificationMapper {
    int deleteByPrimaryKey(Integer id);
    int insert(Notification record);
    List<Notification> selectAll();
    int updateByPrimaryKey(Notification record);
    int read(Integer id);
    List<Notification> selectUnreadComment(Integer userId);
    List<Notification> selectUnreadLike(Integer userId);
}
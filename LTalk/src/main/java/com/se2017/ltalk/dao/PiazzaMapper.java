package com.se2017.ltalk.dao;

import com.se2017.ltalk.entity.Piazza;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface PiazzaMapper {
    int deleteByPrimaryKey(Integer id);
    int insert(Piazza record);
    Piazza selectByPrimaryKey(Integer id);
    List<Piazza> selectAll();
    List<Piazza> selectAllExist();
    List<Piazza> latest();
    List<Piazza> selectByPoster(Integer poster);
    int destroy(Integer id);
    int like(Piazza record);
}
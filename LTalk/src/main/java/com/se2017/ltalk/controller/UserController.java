package com.se2017.ltalk.controller;

import org.apache.xmlrpc.XmlRpcException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.se2017.ltalk.dao.SessionMapper;
import com.se2017.ltalk.entity.Today;
import com.se2017.ltalk.entity.User;
import com.se2017.ltalk.service.CheckerService;
import com.se2017.ltalk.service.UserService;
import com.se2017.ltalk.util.DateUtil;
import com.se2017.ltalk.util.TokenUtil;
import com.se2017.ltalk.util.fileupload.FileFormat;
import com.se2017.ltalk.util.fileupload.ResourceManager;
import com.se2017.ltalk.util.response.Status;

import lombok.extern.log4j.Log4j;

import static com.se2017.ltalk.util.response.Status.*;

import java.io.IOException;
import java.util.Objects;

import static com.se2017.ltalk.util.CheckUtil.*;

@RestController
@RequestMapping(path="user")
@Log4j
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private CheckerService checkerService;
	@Autowired
	private SessionMapper sessionMapper;

	/**
	 * 获取用户信息
	 * @param token
	 * @return
	 */
	@RequestMapping(path="info",method=RequestMethod.GET)
	public Status getInfo(@RequestParam String token){
		if(isNullOrEmpty(token)){
			return ERROR_DATA;
		}
		return success(userService.info(TokenUtil.extract(token).getEmail()));
	}
	/**
	 * 修改用户信息
	 * @param token
	 * @param intro
	 * @param sex
	 * @param nickname
	 * @return
	 */
	@RequestMapping(path="info",method=RequestMethod.POST)
	public Status postInfo(@RequestParam String token,
			@RequestParam(required=false,defaultValue="") String intro,
			@RequestParam(required=false,defaultValue="") String sex,
			@RequestParam(required=false,defaultValue="") String nickname){
		if(isNullOrEmpty(token)||!in(sex, "","g","m")||intro.length()>140||nickname.length()>32){
			return ERROR_DATA;
		}
		return success(userService.info(TokenUtil.extract(token).getEmail(),intro,sex,nickname));
	}
	/**
	 * 修改密码
	 * @param token
	 * @param old
	 * @param current
	 * @param cipher
	 * @param checker
	 * @return
	 */
	@RequestMapping(path="passwd",method=RequestMethod.POST)
	public Status passwd(@RequestParam String token,
			@RequestParam String old,
			@RequestParam String current,
			@RequestParam String cipher,
			@RequestParam String checker){
		if(isNullOrEmpty(token,old,current,cipher,checker)){
			return ERROR_DATA;
		}
		User user=TokenUtil.extract(token);
		int result=checkerService.validate(user.getEmail(), cipher, checker);
		if(result==1){
			return CHECKER_EXPIRED;
		}
		if(result==2){
			return CHECKER_VALIDATE_FAILED;
		}
		if(!(current.length()>=6&&current.length()<=20)){
			return PASSWORD_LENGTH_ERROR;
		}
		String newToken=userService.changePasswd(user.getEmail(),current,old);
		if(newToken==null){
			return AUTHENTICATION_FAILED;
		}
		return Status.success(newToken);
	}
	/**
	 * 上传头像
	 * @param token
	 * @param old
	 * @param current
	 * @param cipher
	 * @param checker
	 * @return
	 */
	@RequestMapping(path="avatar",method=RequestMethod.POST)
	public Status head(@RequestParam MultipartFile avatar,@RequestParam String token) throws IOException{
		if(isNullOrEmpty(token,avatar)){
			return ERROR_DATA;
		}
		User user=TokenUtil.extract(token);
		if(Objects.isNull(user)||!userService.validate(user)){
			return AUTHENTICATION_FAILED;
		}
		if(!userService.exists(user.getEmail())){
			return USER_NULL;
		}
		if(avatar.isEmpty()){
			return FILE_UPLOAD_FAILED;
		}
		if(avatar.getSize()>ResourceManager.FILE_MAX_SIZE){
			return FILE_TOO_LARGE;
		}
		if(!FileFormat.isImage(avatar.getInputStream())){
			return UNSUPPORT_IMAGE_FORMAT;
		}
		String path=null;
		if((path=userService.uploadHead(avatar,user.getEmail()))!=null){
			return success(path);
		}
		return FILE_UPLOAD_FAILED;
	}
	/**
	 * 获取兴趣相同的潜在朋友
	 * @param token
	 * @return
	 */
	@RequestMapping(path="friends",method=RequestMethod.GET)
	public Status friends(@RequestParam String token){
		if(isNullOrEmpty(token)){
			return ERROR_DATA;
		}
		User user=TokenUtil.extract(token);
		if(sessionMapper.selectToday(Today.builder().userId(user.getId()).start(DateUtil.start()).end(DateUtil.end()).build())>20){
			return MATCH_TIME_LIMITED;
		}
		try {
			return success(userService.friends(user.getId()));
		} catch (XmlRpcException e) {
			log.error(e);
			return FAILED;
		}
	}
	/**
	 * 登出
	 * @param token
	 * @return
	 */
	@RequestMapping(path="logout",method=RequestMethod.POST)
	public Status logout(@RequestParam String token){
		return success(userService.logout(TokenUtil.extract(token).getId()));
	}
}

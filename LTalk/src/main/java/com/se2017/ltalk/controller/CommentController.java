package com.se2017.ltalk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.se2017.ltalk.dao.CommentMapper;
import com.se2017.ltalk.entity.Comment;
import com.se2017.ltalk.service.CommentService;
import com.se2017.ltalk.service.UserService;
import com.se2017.ltalk.util.TokenUtil;
import com.se2017.ltalk.util.response.Status;

import lombok.val;

import static com.se2017.ltalk.util.response.Status.*;

import java.util.Objects;

import static com.se2017.ltalk.util.CheckUtil.*;

@RequestMapping(path="piazza")
@RestController
public class CommentController {
	@Autowired
	private CommentService commentService;
	@Autowired
	private UserService userService;
	@Autowired
	private CommentMapper commentMapper;
	/**
	 * 获取评论
	 * @param piazzaId
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(path="{piazzaId}/comment",method=RequestMethod.GET)
	public Status get(@RequestParam String token,@PathVariable int piazzaId,
			@RequestParam(required=false,defaultValue="1") Integer page,
			@RequestParam(required=false,defaultValue="10") Integer rows){
		return success(commentService.get(TokenUtil.extract(token).getId(), piazzaId,page,rows));
	}
	/**
	 * 添加评论
	 * @param piazzaId
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(path="{piazzaId}/comment",method=RequestMethod.POST)
	public Status add(@PathVariable int piazzaId,@RequestParam String token,@RequestParam String content,
			@RequestParam(required=false,defaultValue="0") int pointTo,
			@RequestParam(required=false,defaultValue="0") int commentId){
		if(isNullOrEmpty(token,content)||content.length()>140||pointTo<0||commentId<0){
			return ERROR_DATA;
		}
		if(pointTo!=0){
			if(commentId==0){
				return ERROR_DATA;
			}
			if(!userService.exists(pointTo)){
				return USER_NULL;
			}
			Comment comment=commentMapper.selectByPrimaryKey(commentId);
			if(Objects.isNull(comment)||comment.getReviewer()!=pointTo){
				return ERROR_DATA;
			}
		}
		val user=TokenUtil.extract(token);
		return success(commentService.add(user.getId(),piazzaId,user.getId(),content,pointTo,commentId));
	}
	/**
	 * 点赞
	 * @param piazzaId
	 * @param commentId
	 * @return
	 */
	@RequestMapping(path="{piazzaId}/comment/{commentId}/like",method=RequestMethod.POST)
	public Status like(@RequestParam String token,@PathVariable int piazzaId,@PathVariable int commentId){
		return success(commentService.like(TokenUtil.extract(token).getId(),piazzaId,commentId));
	}
	/**
	 * 取消点赞
	 * @param piazzaId
	 * @param commentId
	 * @return
	 */
	@RequestMapping(path="{piazzaId}/comment/{commentId}/dislike",method=RequestMethod.POST)
	public Status dislike(@RequestParam String token,@PathVariable int piazzaId,@PathVariable int commentId){
		return success(commentService.dislike(TokenUtil.extract(token).getId(),piazzaId,commentId));
	}
}
package com.se2017.ltalk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.se2017.ltalk.dao.PiazzaMapper;
import com.se2017.ltalk.service.PiazzaService;
import com.se2017.ltalk.util.encrypt.AES;

import lombok.val;

@Controller
public class ShareCotroller {
	@Autowired
	private PiazzaMapper piazzaMapper;
	@Autowired
	private PiazzaService piazzaService;
	
	@RequestMapping(path="share/{shareId}",method=RequestMethod.GET)
	public ModelAndView share(@PathVariable String shareId){
		val mv=new ModelAndView("share");
		int piazzaId=-1;
		try{
			String result=AES.decrypt(shareId, AES.KEY);
			piazzaId=Integer.parseInt(result.substring(32, result.length()));
		}
		catch(Throwable t){
			return new ModelAndView("error");
		}
		val piazza=piazzaMapper.selectByPrimaryKey(piazzaId);
		if(piazza==null){
			return new ModelAndView("error");
		}
		mv.addObject("piazza", piazzaService.buildMap(piazza));
		return mv;
	}
}

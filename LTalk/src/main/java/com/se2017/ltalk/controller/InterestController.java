package com.se2017.ltalk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.se2017.ltalk.service.InterestService;
import com.se2017.ltalk.util.TokenUtil;
import com.se2017.ltalk.util.response.Status;
import static com.se2017.ltalk.util.response.Status.*;

import static com.se2017.ltalk.util.CheckUtil.*;

@RestController
public class InterestController {
	@Autowired
	private InterestService interestService;

	/**
	 * 获取所有的兴趣
	 * @return
	 */
	@RequestMapping(path="interest/all",method=RequestMethod.GET)
	public Status all(){
		return Status.success(interestService.all());
	}
	/**
	 * 兴趣的类型
	 * @return
	 */
	@RequestMapping(path="interest/type",method=RequestMethod.GET)
	public Status type(){
		return Status.success(interestService.type());
	}
	/**
	 * 获取某个type下的所有兴趣
	 * @param typeId
	 * @return
	 */
	@RequestMapping(path="interest/type/{name}",method=RequestMethod.GET)
	public Status intrest(@PathVariable String name){
		return Status.success(interestService.type(name));
	}
	/**
	 * 获取用户的兴趣列表
	 * @param name
	 * @return
	 */
	@RequestMapping(path="user/interests",method=RequestMethod.GET)
	public Status get(@RequestParam String token){
		if(isNullOrEmpty(token)){
			return ERROR_DATA;
		}
		return Status.success(interestService.get(TokenUtil.extract(token).getId()));
	}
	/**
	 * 获取用户的兴趣列表
	 * @param name
	 * @return
	 */
	@RequestMapping(path="user/interests",method=RequestMethod.POST)
	public Status add(@RequestParam String token,@RequestParam String interestIds){
		if(isNullOrEmpty(token,interestIds)){
			return ERROR_DATA;
		}
		String[] idsA=interestIds.split(",");
		int[] ids=new int[idsA.length];
		try{
			for(int i=0;i<idsA.length;i++){
				ids[i]=Integer.parseInt(idsA[i]);
			}
		}
		catch(Exception e){
			return ERROR_DATA;
		}
		interestService.add(TokenUtil.extract(token).getId(),ids);
		return Status.success("添加成功");
	}

	/**
	 * 删除用户的兴趣列表
	 * @param name
	 * @return
	 */
	@RequestMapping(path="user/interests/delete",method=RequestMethod.POST)
	public Status delete(@RequestParam String token,@RequestParam String interestIds){
		if(isNullOrEmpty(token,interestIds)){
			return ERROR_DATA;
		}
		String[] idsA=interestIds.split(",");
		int[] ids=new int[idsA.length];
		try{
			for(int i=0;i<idsA.length;i++){
				ids[i]=Integer.parseInt(idsA[i]);
			}
		}
		catch(Exception e){
			return ERROR_DATA;
		}
		if(interestService.delete(TokenUtil.extract(token).getId(),ids)){
			return success("删除成功");
		}
		return FAILED;
	}
}

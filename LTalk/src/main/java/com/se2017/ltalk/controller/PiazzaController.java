package com.se2017.ltalk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.se2017.ltalk.entity.User;
import com.se2017.ltalk.service.PiazzaService;
import com.se2017.ltalk.service.UserService;
import com.se2017.ltalk.util.TokenUtil;
import com.se2017.ltalk.util.fileupload.FileFormat;
import com.se2017.ltalk.util.fileupload.ResourceManager;
import com.se2017.ltalk.util.response.Status;
import static com.se2017.ltalk.util.response.Status.*;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;

@RequestMapping(path="piazza")
@RestController
public class PiazzaController {
	@Autowired
	private PiazzaService piazzaService;
	@Autowired
	private UserService userService;
	
	/**
	 * 获取最新的广场信息
	 * @return
	 */
	@RequestMapping(path="latest",method=RequestMethod.GET)
	public Status latest(@RequestParam String token ,
			@RequestParam(required=false,defaultValue="1") Integer page,
			@RequestParam(required=false,defaultValue="10") Integer rows){
		return success(piazzaService.latest(TokenUtil.extract(token).getId(),page,rows));
	}
	/**
	 * 获取广场信息
	 * @return
	 */
	@RequestMapping(path="{piazzaId}",method=RequestMethod.GET)
	public Status getPiazza(@RequestParam String token ,@PathVariable int piazzaId){
		return success(piazzaService.get(TokenUtil.extract(token).getId(),piazzaId));
	}
	/**
	 * 点赞
	 * @return
	 */
	@RequestMapping(path="{piazzaId}/like",method=RequestMethod.POST)
	public Status like(@RequestParam String token ,@PathVariable int piazzaId){
		return success(piazzaService.like(TokenUtil.extract(token).getId(),piazzaId));
	}
	/**
	 * 获取分享链接
	 * @return
	 */
	@RequestMapping(path="{piazzaId}/share",method=RequestMethod.GET)
	public Status share(@RequestParam String token ,@PathVariable int piazzaId){
		String url=piazzaService.share(TokenUtil.extract(token).getId(),piazzaId);
		return Objects.isNull(url)?FAILED:success(url);
	}
	/**
	 * 取消点赞
	 * @return
	 */
	@RequestMapping(path="{piazzaId}/dislike",method=RequestMethod.POST)
	public Status dislike(@RequestParam String token ,@PathVariable int piazzaId){
		return success(piazzaService.dislike(TokenUtil.extract(token).getId(),piazzaId));
	}
	/**
	 * 添加
	 * @param images
	 * @param token
	 * @param content
	 * @param time
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(path="new",method=RequestMethod.POST)
	public Status add(@RequestParam(required=false,defaultValue="null") MultipartFile[] images,@RequestParam String token,
			@RequestParam(required=false,defaultValue="") String content,@RequestParam long time) throws IOException{
		if(time<0||(!"".equals(content)&&content.length()>140)
				||("".equals(content)&&(images==null||images.length==0))){
			return ERROR_DATA;
		}
		User user=TokenUtil.extract(token);
		if(Objects.isNull(user)||!userService.validate(user)){
			return AUTHENTICATION_FAILED;
		}
		if(!Objects.isNull(images)&&images.length>0){
			for(MultipartFile image:images){
				if(image.isEmpty()){
					return FILE_UPLOAD_FAILED;
				}
				if(image.getSize()>ResourceManager.FILE_MAX_SIZE){
					return FILE_TOO_LARGE;
				}
				if(!FileFormat.isImage(image.getInputStream())){
					return UNSUPPORT_IMAGE_FORMAT;
				}
			}
		}
		Map<String, Object> piazza=null;
		if((piazza=piazzaService.add(user.getId(),images, user.getId(), content, time))!=null){
			return success(piazza);
		}
		return Status.FAILED;
	}

}

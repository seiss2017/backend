package com.se2017.ltalk.controller;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.se2017.ltalk.util.response.Status;
import static com.se2017.ltalk.util.response.Status.*;

/**
 * 全局的错误处理
 * @author w-angler
 *
 */
@RestController
public class ExceptionController{
	/**
	 * 全局的错误处理
	 * @param e
	 * @return
	 */
	@ExceptionHandler({Exception.class})
	public Status exception(Exception e){
		return fail(e);
	}
	/**
	 * 400
	 * @return  
	 */
	@RequestMapping("400")
	public Status error400(){
		return fail("400 bad request");
	}
	/**
	 * 404
	 * @return  
	 */
	@RequestMapping("404")
	public Status error404(){
		return fail("request method not supported");
	}
	/**
	 * 405
	 * @return  
	 */
	@RequestMapping("405")
	public Status error405(){
		return fail("404 not found");
	}
	/**
	 * 500
	 * @return
	 */
	@RequestMapping("500")
	public Status error500(){
		return fail("500 server internal error");
	}
}
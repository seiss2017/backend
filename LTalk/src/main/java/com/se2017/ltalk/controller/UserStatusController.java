package com.se2017.ltalk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.se2017.ltalk.service.CheckerService;
import com.se2017.ltalk.service.UserService;
import com.se2017.ltalk.util.response.Status;
import static com.se2017.ltalk.util.CheckUtil.*;
import static com.se2017.ltalk.util.response.Status.*;

@RestController
public class UserStatusController {
	@Autowired
	private UserService userService;
	@Autowired
	private CheckerService checkerService;
	/**
	 * 登录
	 * @param email
	 * @param passwd
	 * @return
	 */
	@RequestMapping(path="login")
	public Status login(@RequestParam String email,@RequestParam String passwd){
		if(isNullOrEmpty(email,passwd)){
			return ERROR_DATA;
		}
		if(!isEmail(email)){
			return WRONG_EMAIL;
		}
		if(!(passwd.length()>=6&&passwd.length()<=20)){
			return PASSWORD_LENGTH_ERROR;
		}
		String token=userService.login(email,passwd);
		if(token==null){
			return AUTHENTICATION_FAILED;
		}
		return Status.success(token);
	}
	/**
	 * 注册
	 * @param email
	 * @param passwd
	 * @param cipher
	 * @param checker
	 * @return
	 */
	@RequestMapping(path="register")
	public Status register(@RequestParam String email,@RequestParam String passwd,
			@RequestParam String cipher,@RequestParam String checker){
		if(isNullOrEmpty(email,passwd,cipher,checker)){
			return ERROR_DATA;
		}
		if(!isEmail(email)){
			return WRONG_EMAIL;
		}
		if(userService.exists(email)){
			return EMAIL_EXISTS;
		}
		int result=checkerService.validate(email, cipher, checker);
		if(result==1){
			return CHECKER_EXPIRED;
		}
		if(result==2){
			return CHECKER_VALIDATE_FAILED;
		}
		if(!(passwd.length()>=6&&passwd.length()<=20)){
			return PASSWORD_LENGTH_ERROR;
		}
		String token=userService.register(email,passwd,checker);
		if(token==null){
			return AUTHENTICATION_FAILED;
		}
		return Status.success(token);
	}
	/**
	 * 发送验证码
	 * @param email
	 * @return
	 */
	@RequestMapping(path="checker")
	public Status register(@RequestParam String email){
		if(isNullOrEmpty(email)){
			return ERROR_DATA;
		}
		if(!isEmail(email)){
			return WRONG_EMAIL;
		}
		String cipher=checkerService.sendChecker(email);
		if(cipher==null){
			return SEND_EMAIL_FAILED;
		}
		return Status.success(cipher);
	}
	/**
	 * 验证邮箱验证码
	 * @param email
	 * @param cipher
	 * @param checker
	 * @return
	 */
	@RequestMapping(path="validate")
	public Status validate(@RequestParam String email,@RequestParam String cipher,@RequestParam String checker){
		if(isNullOrEmpty(email,cipher,checker)){
			return ERROR_DATA;
		}
		if(!isEmail(email)){
			return WRONG_EMAIL;
		}
		int result=checkerService.validate(email, cipher, checker);
		if(result==1){
			return CHECKER_EXPIRED;
		}
		if(result==2){
			return CHECKER_VALIDATE_FAILED;
		}
		return Status.success("验证成功");
	}
	/**
	 * 忘记密码
	 * @param email
	 * @param passwd
	 * @param cipher
	 * @param checker
	 * @return
	 */
	@RequestMapping(path="forget")
	public Status forget(@RequestParam String email,@RequestParam String passwd,
			@RequestParam String cipher,@RequestParam String checker){
		if(isNullOrEmpty(email,passwd,cipher,checker)){
			return ERROR_DATA;
		}
		if(!isEmail(email)){
			return WRONG_EMAIL;
		}
		if(!userService.exists(email)){
			return USER_NULL;
		}
		int result=checkerService.validate(email, cipher, checker);
		if(result==1){
			return CHECKER_EXPIRED;
		}
		if(result==2){
			return CHECKER_VALIDATE_FAILED;
		}
		if(!(passwd.length()>=6&&passwd.length()<=20)){
			return PASSWORD_LENGTH_ERROR;
		}
		String token=userService.changePasswd(email,passwd,null);
		if(token==null){
			return AUTHENTICATION_FAILED;
		}
		return Status.success(token);
	}
}

package com.se2017.ltalk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.se2017.ltalk.service.InformService;
import com.se2017.ltalk.util.TokenUtil;
import com.se2017.ltalk.util.response.Status;

import static com.se2017.ltalk.util.response.Status.*;

@RestController
public class InformController {
	@Autowired
	private InformService informService;
	@RequestMapping(path="user/inform",method=RequestMethod.POST)
	public Status inform(@RequestParam String token,
			@RequestParam int respondent,@RequestParam String reason){
		if(informService.inform(TokenUtil.extract(token).getId(),respondent,reason)){
			return success("举报成功");
		}
		return FAILED;
	}

}

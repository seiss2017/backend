package com.se2017.ltalk.AI.xiaoi.sdk;

import java.io.Serializable;

import lombok.Data;

@Data
public class RelatedQuestion implements Serializable{
	private static final long serialVersionUID = 1L;
	private String question;
}
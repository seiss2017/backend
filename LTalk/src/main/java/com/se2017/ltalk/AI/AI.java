package com.se2017.ltalk.AI;

import java.io.IOException;
import java.util.Properties;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.se2017.ltalk.AI.xiaoi.sdk.AskRequest;
import com.se2017.ltalk.AI.xiaoi.sdk.AskService;
import com.se2017.ltalk.AI.xiaoi.sdk.CloudNotInitializedException;
import com.se2017.ltalk.AI.xiaoi.sdk.CloudServiceFactory;
import com.se2017.ltalk.AI.xiaoi.utils.Constant;

import lombok.val;

public class AI {
	public static String API_URL;
	public static String API_URL_V2;
	public static String API_KEY;
	static{
		val prop=new Properties();
		val resource=new ClassPathResource("ai.properties");
		try {
			val file = resource.getInputStream();
			prop.load(file);
		} catch (IOException e){
			e.printStackTrace();
		}
		API_URL=prop.getProperty("API_URL");
		API_URL_V2=prop.getProperty("API_URL_V2");
		API_KEY=prop.getProperty("API_KEY");
	}
	/**
	 * http客户端
	 */
	private static CloseableHttpClient client=HttpClients.createDefault();
	/**
	 * JSON转换
	 */
	private static ObjectMapper mapper=new ObjectMapper();
	
	/**
	 * 对话
	 * @return
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 * @throws ClientProtocolException 
	 * @throws Exception
	 */
	public static String ask_v2(String userId,String question) throws ClientProtocolException, JsonProcessingException, IOException{
		String responseStr=null;
		val response=postJson(API_URL_V2,mapper.writeValueAsString(new AIRequest(userId, question)));
		val entity = response.getEntity();
		if (entity != null){
			responseStr=EntityUtils.toString(entity,"UTF-8");
		}
		response.close();
		val result=mapper.readValue(responseStr,AIResponse.class).getResults()[0];
		return (String) result.getValues().get(result.getResultType());
	}
	/**
	 * 对话
	 * @return
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 * @throws ClientProtocolException 
	 * @throws Exception
	 */
	public static String ask(String userId,String question) throws ClientProtocolException, JsonProcessingException, IOException{
		String responseStr=null;
		val response=postJson(API_URL,mapper.writeValueAsString(Request.builder().loc("武汉").info(question).key(API_KEY).userid(userId).build()));
		val entity = response.getEntity();
		if (entity != null){
			responseStr=EntityUtils.toString(entity,"UTF-8");
		}
		response.close();
		return mapper.readValue(responseStr,Response.class).getText();
	}
	
	private static final String APPLICATION_JSON = "application/json";
	private static final String CONTENT_TYPE_TEXT_JSON = "text/json";
	
	/**
	 * 通过POST方法发送JSON数据
	 * @param url
	 * @param json
	 * @return
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 * @throws Exception
	 */
	private static CloseableHttpResponse postJson(String url, String json) throws ClientProtocolException, IOException{
		val httpPost = new HttpPost(url);
		httpPost.addHeader(HTTP.CONTENT_TYPE, APPLICATION_JSON);
		val se = new StringEntity(json);
		se.setContentType(CONTENT_TYPE_TEXT_JSON);
		se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, APPLICATION_JSON));
		httpPost.setEntity(se);
		return client.execute(httpPost);
	}
	
	/***************************小i机器人*******************************/
	private static final String appKey="wHqtfnbQdILv";
	private static final String appSecret="wQgpK5Rx2ovlppCwuJaO";
	private static final AskService askService = CloudServiceFactory.getInstance().createAskService();
	static{
		askService.init(null);
	}
	/**
	 * 使用小i机器人
	 * @param userId
	 * @param question
	 * @return
	 */
	public static String xiaoi(String userId,String question){
		try {
			val result= askService.ask(new AskRequest(appKey, appSecret, question,
					Constant.PRIMARY_TYPE, userId, Constant.WEIXIN_PLATFORM));
			return result.getStatus()==200?result.getContent():"对不起，你说的问题我不太明白";
		} catch (CloudNotInitializedException e) {
			e.printStackTrace();
			return "对不起，你说的问题我不太明白";
		}
	}
	
	@Test
	public void test(){
		for(int i=0;i<10;i++){
			try {
				System.out.println("图灵机器人API："+ask("233", "你好"));
				System.out.println("图灵机器人API_V2："+ask_v2("233", "你好"));
				System.out.println("小i机器人："+xiaoi("233", "你好"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}

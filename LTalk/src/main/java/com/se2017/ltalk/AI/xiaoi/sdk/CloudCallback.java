package com.se2017.ltalk.AI.xiaoi.sdk;

public interface CloudCallback<T>{
  public void success(T paramT);
  public void failed(CloudException paramCloudException);
}

package com.se2017.ltalk.AI.xiaoi.sdk;

public abstract class CloudServiceFactory{
	private static CloudServiceFactory INSTANCE = null;
	public abstract AskService createAskService();
	public static CloudServiceFactory getInstance() {
		if (INSTANCE != null) return INSTANCE;
		CloudServiceFactory factory = null;
		String clazz = System.getProperty(CloudServiceFactory.class.getName(), CloudServiceFactory.class.getName() + "Impl");
		try {
			factory = (CloudServiceFactory)Class.forName(clazz).newInstance();
			if (factory != null) INSTANCE = factory; 
		}
		catch (Exception e) { 
			e.printStackTrace();
			throw new RuntimeException("CloudServiceFactory class not found.", e);
		}
		return factory;
	}
}
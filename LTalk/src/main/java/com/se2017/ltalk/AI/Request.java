package com.se2017.ltalk.AI;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Request {
	private String key;
	private String info;
	private String loc;
	private String userid;
}

package com.se2017.ltalk.AI.xiaoi.sdk;

import java.util.HashMap;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.lang.StringUtils;

import com.se2017.ltalk.AI.xiaoi.utils.AuthUtil;
import com.se2017.ltalk.AI.xiaoi.utils.HttpClientUtil;
import lombok.val;

public class AskServiceImpl implements AskService{
	private String askUrl;
	private String askUri;
	private int connectionTimeout;
	private int readTimeout;
	private HttpClient httpClient;
	private Boolean isInited = Boolean.FALSE;

	public void init(CloudConfiguration config)
	{
		this.isInited = Boolean.TRUE;
		this.askUrl = "http://nlp.xiaoi.com/ask.do";
		this.askUri = "/ask.do";
		this.connectionTimeout = 3000;
		this.readTimeout = 10000;

		if (config != null) {
			if (StringUtils.isNotBlank(config.getUrl()))
				this.askUrl = config.getUrl();
			if (StringUtils.isNotBlank(config.getUri()))
				this.askUri = config.getUri();
			if (config.getConnectionTimeout() > 0)
				this.connectionTimeout = config.getConnectionTimeout();
			if (config.getReadTimeout() > 0) {
				this.readTimeout = config.getReadTimeout();
			}
		}
		this.httpClient = HttpClientUtil.getHttpClient(this.connectionTimeout, this.readTimeout);
	}

	public AskResponse ask(AskRequest request) throws CloudNotInitializedException
	{
		if (!this.isInited.booleanValue()) {
			throw new CloudNotInitializedException();
		}
		String userId = request.getUserId();
		String appKey = request.getAppKey();
		String appSecret = request.getAppSecret();
		String question = request.getQuestion();
		String type = request.getType();
		String platform = request.getPlatform();

		if (StringUtils.isBlank(type)) {
			type = "0";
		}
		String xauth = AuthUtil.generateAuth(appKey, appSecret, 
				"POST", this.askUri);
		val xauthHeader = new HashMap<String, String>();
		xauthHeader.put("X-Auth", xauth);
		val params = new HashMap<String, String>();
		params.put("question", question);
		params.put("userId", userId);
		params.put("type", type);
		params.put("platform", platform);

		val result = HttpClientUtil.doPost(this.httpClient, this.askUrl, params, 
				null, xauthHeader);
		AskResponse askResponse = null;

		if (result != null)
			askResponse = new AskResponse(Integer.parseInt(result.get("status").toString()), (byte[])result.get("responseBody"));
		else {
			askResponse = new AskResponse(404, "".getBytes());
		}
		return askResponse;
	}

	public void destroy()
	{
	}
}
package com.se2017.ltalk.AI.xiaoi.sdk;

import lombok.Data;

@Data
public class CloudConfiguration{
	private String url;
	private String uri;
	private String contentTypeHeader;
	private String xAUEHeader;
	private String xTXEHeader;
	private String xAUFHeader;
	private int connectionTimeout;
	private int readTimeout;
}
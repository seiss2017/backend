package com.se2017.ltalk.AI.xiaoi.sdk;

public interface AskService extends CloudService{
  public AskResponse ask(AskRequest paramAskRequest)throws CloudNotInitializedException;
}
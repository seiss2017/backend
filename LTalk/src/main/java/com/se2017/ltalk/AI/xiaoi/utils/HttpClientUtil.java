package com.se2017.ltalk.AI.xiaoi.utils;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;

import lombok.val;

public class HttpClientUtil{
	public static HttpClient getHttpClient(int connTimeout, int connReadTimeout){
		HttpClient httpClient = null;
		val httpConnectionManager = new MultiThreadedHttpConnectionManager();
		val params = httpConnectionManager.getParams();
		params.setDefaultMaxConnectionsPerHost(100);
		params.setMaxTotalConnections(2147483647);
		params.setConnectionTimeout(connTimeout);
		params.setSoTimeout(connReadTimeout);
		httpClient = new HttpClient(httpConnectionManager);
		httpClient.getParams().setConnectionManagerTimeout(10000L);
		return httpClient;
	}

	public static Map<String, Object> doPost(HttpClient httpClient, String url, Map<String, String> params, String encode, Map<String, String> headers){
		if (params == null)
			throw new IllegalArgumentException("post body should not be null");
		encode = StringUtils.isBlank(encode) ? "utf-8" : encode;
		val post = new PostMethod(url);
		try {
			for (String key : params.keySet()) {
				post.addParameter(new NameValuePair(key, (String)params.get(key)));
			}
			val param = post.getParams();
			param.setContentCharset(encode);
			for (String key : headers.keySet()) {
				post.addRequestHeader(key, (String)headers.get(key));
			}
			int status = httpClient.executeMethod(post);
			val result = new HashMap<String,Object>();
			result.put("status", Integer.valueOf(status));
			result.put("responseBody", post.getResponseBody());
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			post.releaseConnection();
		}
		return null;
	}

	public static Map<String, Object> doPost(HttpClient httpClient, String url, byte[] postBody, String encoding, Map<String, String> headers){
		if (postBody == null) {
			throw new IllegalArgumentException("post body should not be null");
		}
		val post = new PostMethod(url);
		try {
			for (String key : headers.keySet()) {
				post.addRequestHeader(key, (String)headers.get(key));
			}
			post.setRequestEntity(new ByteArrayRequestEntity(postBody));
			int status = httpClient.executeMethod(post);
			val result = new HashMap<String, Object>();
			result.put("status", Integer.valueOf(status));
			result.put("responseBody", post.getResponseBody());
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			post.releaseConnection();
		}
		return null;
	}
}


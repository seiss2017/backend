package com.se2017.ltalk.AI.xiaoi.sdk;

import java.io.Serializable;

import lombok.Data;

@Data
public class Command implements Serializable{
	private static final long serialVersionUID = 1L;
	public static int STATA_NOT_HANDLED = 0;
	public static int STATA_HANDLED = 1;
	private String name;
	private String[] arg;
	private int state = STATA_NOT_HANDLED;
}
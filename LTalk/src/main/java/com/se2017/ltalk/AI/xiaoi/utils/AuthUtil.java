package com.se2017.ltalk.AI.xiaoi.utils;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import lombok.val;

public class AuthUtil{
	public static final String generateAuth(String appKey, String appSecret, String method, String uri){
		val nonce = getNonce();
		val signature = getSignature(appKey, null, appSecret, method, nonce, uri);
		val XAuth = "app_key=\"" + appKey + "\", nonce=\"" + nonce + 
				"\", signature=\"" + signature + "\"";
		return XAuth;
	}

	@SuppressWarnings("deprecation")
	public static final String getSignature(String appKey, String realm, String appSecret, String method, String nonce, String uri){
		if (realm == null)
			realm = "xiaoi.com";
		val HA1 = DigestUtils.shaHex(StringUtils.join(new String[] { appKey, 
				realm, appSecret }, ":"));
		val HA2 = DigestUtils.shaHex(StringUtils.join(new String[] { method, 
				uri }, ":"));
		return DigestUtils.shaHex(StringUtils.join(new String[] { HA1, nonce, 
				HA2 }, ":"));
	}

	public static final String getNonce() {
		return "__" + new String(Hex.encodeHex(new StringBuilder().append(System.currentTimeMillis())
				.toString().getBytes()));
	}
}

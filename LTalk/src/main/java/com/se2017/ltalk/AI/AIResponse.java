package com.se2017.ltalk.AI;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 响应信息
 * @author w-angler
 *
 */
@Data
public class AIResponse {
	private Intent Intent;
	private Result[] results;

	@Builder
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class Intent{
		private int code;
		private String intentName;
		private String actionName;
		private Map<String,Object> parameters;
	}
	@Builder
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class Result{
		private String resultType;
		private Map<String,Object> values;
	}
}

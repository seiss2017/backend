package com.se2017.ltalk.AI.xiaoi.sdk;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AskRequest{
	private String appKey;
	private String appSecret;
	private String question;
	private String type;
	private String userId;
	private String platform;
}
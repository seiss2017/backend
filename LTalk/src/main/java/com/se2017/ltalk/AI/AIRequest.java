package com.se2017.ltalk.AI;

import lombok.Builder;
import lombok.Data;

/**
 * 请求信息
 * @author w-angler
 *
 */
@Data
public class AIRequest {
	//输入信息
	private Perception perception;
	//用户参数
	private UserInfo userInfo;
	
	public AIRequest(String userId,String message){
		perception=Perception.builder()
				.inputText(InputText.builder().text(message).build())
				.selfInfo(SelfInfo.builder().build())
				.build();
		userInfo=UserInfo.builder().apiKey(AI.API_KEY).userId(userId).build();
	}
	/**
	 * 输入信息
	 */
	@Builder
	@Data
	public static class Perception{
		private InputText inputText;
		private SelfInfo selfInfo;
	}
	/**
	 * 文本信息
	 */
	@Builder
	@Data
	public static class InputText{
		private String text;//直接输入文本
	}
	/**
	 * 客户端属性
	 */
	@Builder
	@Data
	public static class SelfInfo{
		private Location location;//地理位置信息
	}
	/**
	 * 地理位置信息
	 */
	@Builder
	@Data
	public static class Location{
		private String city;//所在城市
		private String latitude;//纬度，大于0为北纬，小于0为南纬
		private String longitude;//经度，大于0为东经，小于0为西经
		private String nearest_poi_name;//最近街道名称
		private String province;//省份
		private String street;//街道
	}
	/**
	 * 用户参数
	 */
	@Builder
	@Data
	public static class UserInfo{
		private String apiKey;//场景标识
		private String userId;//用户唯一标识
	}
}

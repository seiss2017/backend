package com.se2017.ltalk.AI.xiaoi.utils;

public class Constant{
  public static final String ENCODING = "utf-8";
  public static final String WEIXIN_PLATFORM = "weixin";
  public static final String YIXIN_PLATFORM = "yixin";
  public static final String SINAWEIBO_PLATFORM = "wbfan";
  public static final String IOS_PLATFORM = "ios";
  public static final String ANDROID_PLATFORM = "android";
  public static final String CUSTOM_PLATFORM = "custom";
  public static final String POST_METHOD = "POST";
  public static final String GET_METHOD = "GET";
  public static final String ASK_URI = "/ask.do";
  public static final String RECOG_URI = "/recog.do";
  public static final String SYNTH_URI = "/synth.do";
  public static final String REALM = "xiaoi.com";
  public static final String ASK_URL = "http://nlp.xiaoi.com/ask.do";
  public static final String RECOG_URL = "http://vcloud.xiaoi.com/recog.do";
  public static final String SYNTH_URL = "http://vcloud.xiaoi.com/synth.do";
  public static final String ENCHARS = "abcdefghijklmnopqrstuvwxyz";
  public static final String PRIMARY_TYPE = "0";
  public static final String SENIOR_TYPE = "1";
  public static final String RECOG_CONTENTTYPE_HEADER = "audio/speex";
  public static final String SYNTH_CONTENTTYPE_HEADER = "text/plain";
  public static final String XAUE_HEADER = "speex";
  public static final String XTXE_HEADER = "UTF-8";
  public static final String XAUF_HEADER = "audio/L16;rate=16000";
  public static final int CONNECTION_TIMEOUT = 3000;
  public static final int READ_TIMEOUT = 10000;
}
package com.se2017.ltalk.AI.xiaoi.sdk;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class AskResponse implements ResponseRender{
	private int status;
	@SuppressWarnings("unused")
	private byte[] body;
	private int type = -1;
	private String content;
	private String words;
	private float similarity;
	private Command[] commands;
	private RelatedQuestion[] relatedQuestions;

	public AskResponse(int status, byte[] body){
		render(status, body);
	}

	public void render(int httpStatus, byte[] body){
		this.status = httpStatus;
		String bodyStr = null;
		try {
			bodyStr = new String(body, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		if (this.status == 200) {
			this.content = bodyStr;
			if (this.content.indexOf("?xml") > -1)
				try {
					parseXml();
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
		else if (this.status == 401) {
			this.content = 
					("auth result:" + 
							bodyStr.substring(bodyStr.indexOf("auth result:") + 13, 
									bodyStr.indexOf("auth result:") + 14));
		} else {
			this.content = "server error";
		}
	}

	public void parseXml() throws IOException, JDOMException {
		SAXBuilder builder = new SAXBuilder();
		Document doc = builder.build(new ByteArrayInputStream(this.content.getBytes("UTF-8")));
		Element responseEle = doc.getRootElement();
		String typeText = responseEle.getChildTextTrim("Type");
		if (StringUtils.isNotBlank(typeText)) {
			this.type = Integer.parseInt(typeText);
		}
		String contentText = responseEle.getChildTextTrim("Content");
		if (StringUtils.isNotBlank(contentText)) {
			this.content = contentText;
		}
		String similarityText = responseEle.getChildTextTrim("Similarity");
		if (StringUtils.isNotBlank(similarityText)) {
			this.similarity = Float.parseFloat(similarityText);
		}
		String wordsText = responseEle.getChildTextTrim("Words");
		if (StringUtils.isNotBlank(wordsText)) {
			this.words = wordsText;
		}
		Element commandsEle = responseEle.getChild("Commands");
		if (commandsEle != null) {
			List<?> commandList = commandsEle.getChildren("Command");
			if ((commandList != null) && (!commandList.isEmpty())) {
				this.commands = new Command[commandList.size()];
				for (int i = 0; i < commandList.size(); i++) {
					Element commandEle = (Element)commandList.get(i);
					Command commend = new Command();
					String nameAttrValue = commandEle.getAttributeValue("name");
					if (StringUtils.isNotBlank(nameAttrValue)) {
						commend.setName(nameAttrValue);
					}
					String stateAttrValue = commandEle.getAttributeValue("state");
					if (StringUtils.isNotBlank(stateAttrValue)) {
						commend.setState(Integer.parseInt(stateAttrValue));
					}
					List<?> argList = commandEle.getChildren("Arg");
					if ((argList != null) && (!argList.isEmpty())) {
						String[] arg = new String[argList.size()];
						for (int j = 0; j < argList.size(); j++) {
							Element argEle = (Element)argList.get(j);
							arg[j] = argEle.getTextTrim();
						}
						commend.setArg(arg);
					}
					this.commands[i] = commend;
				}
			}
		}
		Element relatedQuestionsEle = responseEle.getChild("RelatedQuestions");
		if (relatedQuestionsEle != null) {
			List<?> questionList = relatedQuestionsEle.getChildren("Question");
			if ((questionList != null) && (!questionList.isEmpty())) {
				this.relatedQuestions = new RelatedQuestion[questionList.size()];
				for (int i = 0; i < questionList.size(); i++) {
					Element questionEle = (Element)questionList.get(i);
					if ((questionEle != null) && (StringUtils.isNotBlank(questionEle.getTextTrim()))) {
						RelatedQuestion rq = new RelatedQuestion();
						rq.setQuestion(questionEle.getTextTrim());
						this.relatedQuestions[i] = rq;
					}
				}
			}
		}
	}

	public int getStatus() {
		return this.status;
	}

	public String getContent() {
		return this.content;
	}

	public int getType() {
		return this.type;
	}

	public String getWords() {
		return this.words;
	}

	public float getSimilarity() {
		return this.similarity;
	}

	public Command[] getCommands() {
		return this.commands;
	}

	public RelatedQuestion[] getRelatedQuestions() {
		return this.relatedQuestions;
	}

	public String toString()
	{
		return super.toString();
	}
}
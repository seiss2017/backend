package com.se2017.ltalk.util;

import com.se2017.ltalk.service.PiazzaService;

import lombok.extern.log4j.Log4j;

@Log4j
public class AutoDestroy {
	private PiazzaService piazzaService;
	public PiazzaService getPiazzaService() {
		return piazzaService;
	}
	public void setPiazzaService(PiazzaService piazzaService) {
		this.piazzaService = piazzaService;
	}
	public void destory(){
		log.info("------------------------begin------------------------");
		int sum=piazzaService.destroy();
		log.info(sum+" destroyed");
		log.info("------------------------end------------------------");
	}
	public void cancel(){
		
	}
}

package com.se2017.ltalk.util.fileupload;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.springframework.core.io.ClassPathResource;
import org.springframework.web.multipart.MultipartFile;
import com.se2017.ltalk.util.encrypt.MD5;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.val;

/**
 * 文件上传
 * @author w-angler
 *
 */

public class ResourceManager {
	/**
	 * 可以上传的最大文件大小（字节数）
	 */
	public static final int FILE_MAX_SIZE=10240000;
	/**
	 * 根目录
	 */
	private static String basePath;
	/**
	 * 头像
	 */
	private static String userHead;
	/**
	 * 广场图片
	 */
	private static String piazzaImage;
	/**
	 * 资源访问的URL
	 */
	public static String urlBase;
	public static String shareBase;

	//加载配置
	static{
		val prop=new Properties();
		val resource=new ClassPathResource("fileupload.properties");
		try {
			prop.load(resource.getInputStream());
			basePath=prop.getProperty("base.path");
			userHead=prop.getProperty("user.head");
			piazzaImage=prop.getProperty("piazza.image");
			urlBase=prop.getProperty("url.base");
			shareBase=prop.getProperty("share.base");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 禁止实例化
	 */
	private ResourceManager(){
		throw new RuntimeException("You can not new an instance of this class!");
	}
	/**
	 * 保存文件
	 */
	public static String savePiazzaImage(MultipartFile file,String replace) throws IOException{
		val fileItem=ResourceManager.parse(file);
		val url=(piazzaImage+fileItem.getFileName()).replace("{placeholder}", replace);
		ResourceManager.save(fileItem.getFileContent(),url);
		return urlBase+url;
	}
	/**
	 * 保存头像
	 * @param fileItem
	 * @param path
	 * @throws IOException
	 */
	public static String saveHead(MultipartFile file,String name) throws IOException{
		val saveName=name+"."+FileFormat.judge(file.getInputStream()).toString().toLowerCase();
		val url=userHead+saveName;
		ResourceManager.save(file.getBytes(),url);
		return urlBase+url;
	}
	
	private static void save(byte[] content,String path) throws IOException{
		val file=new File(ResourceManager.basePath+path);
		//如果文件存在，则删除
		if(file.exists()){
			file.delete();
		}
		if(!file.getParentFile().exists()){
			file.getParentFile().mkdirs();
		}
		file.createNewFile();
		val out=new FileOutputStream(file);
		out.write(content);
		out.flush();
		out.close();
	}
	/**
	 * 解析上传的文件
	 * @param file
	 * @return
	 * @throws IOException 
	 */
	private static FileItem parse(MultipartFile file) throws IOException{
		return new FileItem(file.getBytes(),
				MD5.hash(file.getOriginalFilename())+System.currentTimeMillis()+"."
						+FileFormat.judge(file.getInputStream()).toString().toLowerCase());
	}
	@Data
	@AllArgsConstructor
	private static class FileItem{
		private byte[] fileContent;
		private String fileName;
	}
}

package com.se2017.ltalk.util.encrypt;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import lombok.val;

public class MD5 {
	private MD5(){}

	private static final char[] HEX="0123456789abcdef".toCharArray();
	
	public static String hash(String s){
		String code=null;
		try {
			val md5=MessageDigest.getInstance("MD5");
			md5.update(s.getBytes("UTF-8"));
			byte[] array=md5.digest();
			code=getString(array);
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return code;
	}
	/**
	 * 将加密后的字节数组转化为字符串
	 * @param array 加密后的字节数组
	 * @return
	 */
	private static String getString(byte[] array){
		val sb=new StringBuffer();
		for(byte b:array){
			int temp=b;
			if(temp<0){
				temp=temp+256;
			}
			int d1=temp/16;
			int d2=temp%16;
			sb.append(HEX[d1]).append(HEX[d2]);
		}
		return sb.toString();
	}
}
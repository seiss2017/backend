package com.se2017.ltalk.util.encrypt;

import java.math.BigInteger;
import java.security.KeyFactory;  
import java.security.KeyPairGenerator;  
import java.security.NoSuchAlgorithmException;  
import java.security.interfaces.RSAPrivateKey;  
import java.security.interfaces.RSAPublicKey;  
import java.security.spec.RSAPrivateKeySpec;  
import java.security.spec.RSAPublicKeySpec;  
import java.util.HashMap;  

import javax.crypto.Cipher;

import lombok.val;  

public class RSA {  
	private RSA(){}

	public static void main(String[] args) throws Exception {
		val map = RSA.getKeys();  
		//生成公钥和私钥  
		val publicKey = (RSAPublicKey) map.get("public");  
		val privateKey = (RSAPrivateKey) map.get("private");  

		//模  
		val modulus = publicKey.getModulus().toString();  
		System.out.println(modulus);
		//公钥指数  
		val public_exponent = publicKey.getPublicExponent().toString();  
		System.out.println(public_exponent);
		//私钥指数  
		val private_exponent = privateKey.getPrivateExponent().toString();
		System.out.println(private_exponent);
		//明文  
		String ming = "123456789";  
		//使用模和指数生成公钥和私钥  
		val pubKey = RSA.getPublicKey(modulus, public_exponent);  
		System.out.println();
		val priKey = RSA.getPrivateKey(modulus, private_exponent);  
		//加密后的密文  
		val mi = RSA.encryptByPublicKey(ming, pubKey);  
		System.err.println(mi);  
		//解密后的明文  
		ming = RSA.decryptByPrivateKey(mi, priKey);  
		System.err.println(ming);  
	}
	/** 
	 * 生成公钥和私钥 
	 * @throws NoSuchAlgorithmException  
	 * 
	 */  
	public static HashMap<String, Object> getKeys() throws NoSuchAlgorithmException{  
		val map = new HashMap<String, Object>();  
		val keyPairGen = KeyPairGenerator.getInstance("RSA");  
		keyPairGen.initialize(1024);  
		val keyPair = keyPairGen.generateKeyPair();  
		val publicKey = (RSAPublicKey) keyPair.getPublic();  
		val privateKey = (RSAPrivateKey) keyPair.getPrivate();  
		map.put("public", publicKey);  
		map.put("private", privateKey);  
		return map;  
	}  
	/** 
	 * 使用模和指数生成RSA公钥 
	 * 注意：【此代码用了默认补位方式，为RSA/None/PKCS1Padding，不同JDK默认的补位方式可能不同，如Android默认是RSA 
	 * /None/NoPadding】 
	 *  
	 * @param modulus 
	 *            模 
	 * @param exponent 
	 *            指数 
	 * @return 
	 */  
	public static RSAPublicKey getPublicKey(String modulus, String exponent) {  
		try {  
			val b1 = new BigInteger(modulus);  
			val b2 = new BigInteger(exponent);  
			val keyFactory = KeyFactory.getInstance("RSA");  
			val keySpec = new RSAPublicKeySpec(b1, b2);  
			return (RSAPublicKey) keyFactory.generatePublic(keySpec);  
		} catch (Exception e) {  
			e.printStackTrace();  
			return null;  
		}  
	}  

	/** 
	 * 使用模和指数生成RSA私钥 
	 * 注意：【此代码用了默认补位方式，为RSA/None/PKCS1Padding，不同JDK默认的补位方式可能不同，如Android默认是RSA 
	 * /None/NoPadding】 
	 *  
	 * @param modulus 
	 *            模 
	 * @param exponent 
	 *            指数 
	 * @return 
	 */  
	public static RSAPrivateKey getPrivateKey(String modulus, String exponent) {  
		try {  
			val b1 = new BigInteger(modulus);  
			val b2 = new BigInteger(exponent);  
			val keyFactory = KeyFactory.getInstance("RSA");  
			val keySpec = new RSAPrivateKeySpec(b1, b2);  
			return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);  
		} catch (Exception e) {  
			e.printStackTrace();  
			return null;  
		}  
	}  

	/** 
	 * 公钥加密 
	 *  
	 * @param data 
	 * @param publicKey 
	 * @return 
	 * @throws Exception 
	 */  
	public static String encryptByPublicKey(String data, RSAPublicKey publicKey)  
			throws Exception {  
		val cipher = Cipher.getInstance("RSA");  
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);  
		// 模长  
		val key_len = publicKey.getModulus().bitLength() / 8;  
		// 加密数据长度 <= 模长-11  
		val datas = splitString(data, key_len - 11);  
		String mi = "";  
		//如果明文长度大于模长-11则要分组加密  
		for (String s : datas) {  
			mi += bcd2Str(cipher.doFinal(s.getBytes()));  
		}  
		return mi;  
	}  

	/** 
	 * 私钥解密 
	 *  
	 * @param data 
	 * @param privateKey 
	 * @return 
	 * @throws Exception 
	 */  
	public static String decryptByPrivateKey(String data, RSAPrivateKey privateKey)  
			throws Exception {  
		val cipher = Cipher.getInstance("RSA");  
		cipher.init(Cipher.DECRYPT_MODE, privateKey);  
		//模长  
		val key_len = privateKey.getModulus().bitLength() / 8;  
		val bytes = data.getBytes();  
		val bcd = ASCII_To_BCD(bytes, bytes.length);  
		System.err.println(bcd.length);  
		//如果密文长度大于模长则要分组解密  
		String ming = "";  
		val arrays = splitArray(bcd, key_len);  
		for(byte[] arr : arrays){  
			ming += new String(cipher.doFinal(arr));  
		}  
		return ming;  
	}  
	/** 
	 * ASCII码转BCD码 
	 *  
	 */  
	public static byte[] ASCII_To_BCD(byte[] ascii, int asc_len) {  
		val bcd = new byte[asc_len / 2];  
		int j = 0;  
		for (int i = 0; i < (asc_len + 1) / 2; i++) {  
			bcd[i] = asc_to_bcd(ascii[j++]);  
			bcd[i] = (byte) (((j >= asc_len) ? 0x00 : asc_to_bcd(ascii[j++])) + (bcd[i] << 4));  
		}  
		return bcd;  
	}  
	public static byte asc_to_bcd(byte asc) {  
		byte bcd;  

		if ((asc >= '0') && (asc <= '9'))  
			bcd = (byte) (asc - '0');  
		else if ((asc >= 'A') && (asc <= 'F'))  
			bcd = (byte) (asc - 'A' + 10);  
		else if ((asc >= 'a') && (asc <= 'f'))  
			bcd = (byte) (asc - 'a' + 10);  
		else  
			bcd = (byte) (asc - 48);  
		return bcd;  
	}  
	/** 
	 * BCD转字符串 
	 */  
	public static String bcd2Str(byte[] bytes) {  
		char temp[] = new char[bytes.length * 2], val;  

		for (int i = 0; i < bytes.length; i++) {  
			val = (char) (((bytes[i] & 0xf0) >> 4) & 0x0f);  
			temp[i * 2] = (char) (val > 9 ? val + 'A' - 10 : val + '0');  

			val = (char) (bytes[i] & 0x0f);  
			temp[i * 2 + 1] = (char) (val > 9 ? val + 'A' - 10 : val + '0');  
		}  
		return new String(temp);  
	}  
	/** 
	 * 拆分字符串 
	 */  
	public static String[] splitString(String string, int len) {  
		val x = string.length() / len;  
		val y = string.length() % len;  
		int z = 0;  
		if (y != 0) {  
			z = 1;  
		}  
		val strings = new String[x + z];  
		String str = "";  
		for (int i=0; i<x+z; i++) {  
			if (i==x+z-1 && y!=0) {  
				str = string.substring(i*len, i*len+y);  
			}else{  
				str = string.substring(i*len, i*len+len);  
			}  
			strings[i] = str;  
		}  
		return strings;  
	}  
	/** 
	 *拆分数组  
	 */  
	public static byte[][] splitArray(byte[] data,int len){  
		val x = data.length / len;  
		val y = data.length % len;  
		int z = 0;  
		if(y!=0){  
			z = 1;  
		}  
		val arrays = new byte[x+z][];  
		byte[] arr;  
		for(int i=0; i<x+z; i++){  
			arr = new byte[len];  
			if(i==x+z-1 && y!=0){  
				System.arraycopy(data, i*len, arr, 0, y);  
			}else{  
				System.arraycopy(data, i*len, arr, 0, len);  
			}  
			arrays[i] = arr;  
		}  
		return arrays;  
	}  
}  

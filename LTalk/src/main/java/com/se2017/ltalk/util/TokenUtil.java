package com.se2017.ltalk.util;

import java.util.Objects;
import java.util.Timer;
import java.util.UUID;

import com.se2017.ltalk.entity.User;
import com.se2017.ltalk.util.encrypt.AES;

import lombok.val;

public final class TokenUtil {
	//定时器
	private static Timer timer=new Timer();
	static{
		//TODO 每隔3天更新一次AES密钥
		timer.toString();
	}
	/**
	 * 生成Token（目前不安全，以后需要修正）
	 * @param email
	 * @return
	 */
	public static String generate(User user) {
		return AES.encrypt(String.format("%010d", user.getId())+UUID.randomUUID().toString().substring(0, 8)+user.getEmail(),AES.KEY);
	}
	/**
	 * 从Token中提取出加密的信息
	 * @param token
	 * @return
	 */
	public static User extract(String token) {
		val result=AES.decrypt(token, AES.KEY);
		return Objects.isNull(result)?null:User.builder().id(Integer.parseInt(result.substring(0,10))).token(token).email(result.substring(18,result.length())).build();
	}
}

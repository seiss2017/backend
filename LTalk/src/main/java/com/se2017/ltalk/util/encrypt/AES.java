package com.se2017.ltalk.util.encrypt;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import lombok.val;

public class AES {
	private  AES(){}
	public static final String KEY="LTALK@#$%^&*()-+";
	private static final Base64.Encoder encoder=Base64.getUrlEncoder();
	private static final Base64.Decoder decoder=Base64.getUrlDecoder();
    /**
     * 加密
     * @param sSrc
     * @param sKey
     * @return
     * @throws Exception
     */
    public static String encrypt(String sSrc, String sKey){
		try {
	        if (sKey == null) {
	            System.out.print("Key为空null");
	            return null;
	        }
	        if (sKey.length() != 16) {
	            System.out.print("Key长度不是16位");
	            return null;
	        }
	        val cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");//"算法/模式/补码方式"
	        cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(sKey.getBytes("UTF-8"), "AES"));
	        return new String(encoder.encode(cipher.doFinal(sSrc.getBytes("UTF-8"))),"UTF-8");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
    }
    /**
     * 解密
     * @param sSrc
     * @param sKey
     * @return
     * @throws Exception
     */
    public static String decrypt(String sSrc, String sKey){
        try {
            if (sKey == null) {
                System.out.print("Key为空null");
                return null;
            }
            if (sKey.length() != 16) {
                System.out.print("Key长度不是16位");
                return null;
            }
            try {
            	val cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
                cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(sKey.getBytes("UTF-8"), "AES"));
                return new String(cipher.doFinal(decoder.decode(sSrc.getBytes("UTF-8"))),"UTF-8");
            } catch (Exception e) {
                System.out.println(e.toString());
                return null;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
}
package com.se2017.ltalk.util.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Status {
	/**
	 * 请求结果
	 */
	private boolean result;
	
	/**
	 * 状态码
	 */
	private int statusCode;
	
	/**
	 * 需要让用户知道的数据
	 */
	private Object data;
	/**
	 * 请求成功
	 * @param data 请求的数据
	 * @return
	 */
	public static Status success(Object data){
		return new Status(true, StatusCode.SUCCESS, data);
	}
	public static final Status FAILED=new Status(false, StatusCode.FAILED, null);
	public static Status fail(Object data){
		return new Status(false, StatusCode.FAILED, data);
	}
	/**
	 * 认证错误
	 * @return
	 */
	public static final Status AUTHENTICATION_FAILED=new Status(false, StatusCode.AUTHENTICATION_FAILED, null);
	/**
	 * 参数错误
	 * @return
	 */
	public static final Status ERROR_DATA=new Status(false, StatusCode.ERROR_DATA, null);
	/**
	 * 用户名错误（用户不存在）
	 * @return
	 */
	public static final Status USER_NULL=new Status(false, StatusCode.USER_NULL, null);
	/**
	 * 用户名已注册
	 * @return
	 */
	public static final Status USER_EXISTS=new Status(false, StatusCode.USER_EXISTS, null);
	/**
	 * 密码错误
	 * @return
	 */
	public static final Status PASSWORD_ERROR=new Status(false, StatusCode.PASSWORD_ERROR, null);
	/**
	 * 密码长度错误（6-16）
	 * @return
	 */
	public static final Status PASSWORD_LENGTH_ERROR=new Status(false, StatusCode.PASSWORD_LENGTH_ERROR, null);
	/**
	 * 文件上传失败
	 * @return
	 */
	public static final Status FILE_UPLOAD_FAILED=new Status(false, StatusCode.FILE_UPLOAD_FAILED, null);
	/**
	 * 文件过大
	 * @return
	 */
	public static final Status FILE_TOO_LARGE=new Status(false, StatusCode.FILE_TOO_LARGE, null);
	/**
	 * 不支持的图片格式
	 * @return
	 */
	public static final Status UNSUPPORT_IMAGE_FORMAT=new Status(false, StatusCode.UNSUPPORT_IMAGE_FORMAT, null);
	/**
	 * 邮箱错误
	 * @return
	 */
	public static final Status WRONG_EMAIL=new Status(false, StatusCode.WRONG_EMAIL, null);
	/**
	 * 邮箱已存在
	 * @return
	 */
	public static final Status EMAIL_EXISTS= new Status(false, StatusCode.EMAIL_EXISTS, null);
	/**
	 * 验证码验证失败
	 * @return
	 */
	public static final Status CHECKER_VALIDATE_FAILED= new Status(false, StatusCode.CHECKER_VALIDATE_FAILED, null);
	/**
	 * 验证码过期
	 * @return
	 */
	public static final Status CHECKER_EXPIRED= new Status(false, StatusCode.CHECKER_EXPIRED, null);
	/**
	 * 邮件发送失败
	 * @return
	 */
	public static final Status SEND_EMAIL_FAILED=new Status(false, StatusCode.SEND_EMAIL_FAILED, null);
	/**
	 * 邮每天的匹配次数已用完
	 * @return
	 */
	public static final Status MATCH_TIME_LIMITED=new Status(false, StatusCode.MATCH_TIME_LIMITED, null);
}

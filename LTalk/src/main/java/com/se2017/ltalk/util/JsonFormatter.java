package com.se2017.ltalk.util;

import lombok.val;

/**
 * json字符串格式化
 * @author w-angler
 *
 */
public final class JsonFormatter {
	private JsonFormatter(){
		throw new IllegalAccessError();
	}
	/**
	 * 格式化JSON数据
	 * @param json
	 * @return
	 */
	public static String pretty(String json) {
        if (null == json || "".equals(json)){
        	return "";
        }
        val pretty = new StringBuilder();
        char last = '\0';
        char current = '\0';
        int indent = 0;
        for (int i = 0; i < json.length(); i++) {
            last = current;
            current = json.charAt(i);
            switch (current) {
                case '{':
                case '[':
                	pretty.append(current);
                	pretty.append('\n');
                    indent++;
                    indent(pretty, indent);
                    break;
                case '}':
                case ']':
                	pretty.append('\n');
                    indent--;
                    indent(pretty, indent);
                    pretty.append(current);
                    break;
                case ',':
                	pretty.append(current);
                    if (last != '\\') {
                    	pretty.append('\n');
                        indent(pretty, indent);
                    }
                    break;
                default:
                	pretty.append(current);
            }
        }
        return pretty.toString();
    }
	/**
	 * 添加空格
	 * @param sb
	 * @param indent
	 */
	private static void indent(StringBuilder pretty,int indent){
        for (int i = 0; i < indent; i++) pretty.append("    ");
    }
}

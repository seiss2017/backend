package com.se2017.ltalk.entity;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Session {
    private int id;
    private int sponsor;
    private int recipient;
    private long lastTime;
    private LocalDateTime createTime;
    private boolean isDeleted;
}
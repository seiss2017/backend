package com.se2017.ltalk.entity;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Piazza {
    private int id;
    private int poster;
    private String content;
    private String images;
    private int likes;
    private LocalDateTime expireTime;
    private boolean isDestroyed;
    private LocalDateTime createTime;
    private boolean isDeleted;
}
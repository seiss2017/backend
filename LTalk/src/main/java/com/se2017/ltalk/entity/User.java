package com.se2017.ltalk.entity;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private int id;
    private boolean sex;
    private String email;
    private String nickname;
    private String avatar;
    private String passwd;
    private String intro;
    private String token;
    private LocalDateTime createTime;
    private boolean isDeleted;
}
package com.se2017.ltalk.entity;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Today {
	private int userId;
	private LocalDateTime start;
	private LocalDateTime end;
}

package com.se2017.ltalk.entity;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Notification {
    private int id;
    private int receiver;
    private int type;
    private String content;
    private LocalDateTime createTime;
    private boolean isRead;
    private boolean isDeleted;
}
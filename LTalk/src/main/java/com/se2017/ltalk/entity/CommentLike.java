package com.se2017.ltalk.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommentLike {
    private int id;
    private int userId;
    private int commentId;
    private boolean isDeleted;
}
package com.se2017.ltalk.entity;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Comment {
    private int id;
    private int piazzaId;
    private String content;
    private int reviewer;
    private int likes;
    private int pointTo;
    private LocalDateTime createTime;
    private boolean isDeleted;
}
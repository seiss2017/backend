package com.se2017.ltalk.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Inform {
    private Integer id;
    private Integer informer;
    private Integer respondent;
    private String reason;
}
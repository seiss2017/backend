package com.se2017.ltalk.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PiazzaLike {
    private int id;
    private int userId;
    private int piazzaId;
    private boolean isDeleted;
}
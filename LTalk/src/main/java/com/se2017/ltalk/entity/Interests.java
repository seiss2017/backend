package com.se2017.ltalk.entity;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Interests {
    private int id;
    private int userId;
    private int interestId;
    private LocalDateTime createTime;
    private boolean isDeleted;
}